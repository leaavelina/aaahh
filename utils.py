#!/usr/bin/env python

import glob
import os
from datetime import datetime
import time
import re

import torch


def timestr(secs:int, t:str=None):
    """ Takes a number of seconds `ts` to format the output depending on type `t` """
    # Point in time
    if t == "file":
        dt = datetime.fromtimestamp(secs)
        return dt.strftime("%Y%m%d_%H%M%S")
    elif t == "date":
        dt = datetime.fromtimestamp(secs)
        return dt.strftime("%a, %d.%m.%Y %H:%M:%S")

    # Time period / duration
    h, s = divmod(secs, 3600)
    m, s = divmod(s, 60)
    if t == "sentence":
        return "{}h {}m {}s".format(int(h), int(m), int(s))
    elif t == "clock":
        return "{:02d}:{:02d}:{:02d}".format(int(h), int(m), int(s))
    elif t == 's' or t == 'sec' or t == 'seconds':
        return f"{int(secs)} seconds"
    elif t == 'm' or t == 'min' or t == 'minutes':
        return f"{int(divmod(secs, 60)[0])} minutes"
    elif t == 'h' or t == 'hrs' or t == 'hours':
        return f"{int(h)} hours"
    
    return str(secs)
    

def get_run_nr(root_dir:str, comment:str=None, last:bool=False):
    """ Returns the number of the next or last run in a given directory for a given comment. """
    runs = glob.glob(os.path.join(root_dir +"/*/"))
    if last:
        comment = "" if comment is None else comment
        runs_ = [r for r in runs if str(comment) in r]
        if len(runs_) == 0:  # There are no directories matching comment, not overwriting anything
            last = False
        else: # Get the last number only of those directories matching comment
            runs = runs_  
    runs = [r[len(root_dir):].split("_", 1) for r in runs]
    runs = [[int(nr), comment] for nr, comment in runs if nr.isdigit()]
    runs.sort(key=lambda x: int(x[0]), reverse=True)

    if len(runs) < 1: return 1
    run = runs[0] if last else (runs[0][0]+1, None)
    return run  #= (run_nr, run_comment)

def get_run_dirs(model_root_dir:str, log_root_dir:str, comment:str, use_existing:bool=False):
    """ Returns a unique directory path for logging and model-files,
    or returns the directories to the last run with that comment. """
    model_number, mdl_cmt = get_run_nr(root_dir=model_root_dir, comment=comment, last=use_existing)
    log_number, log_cmt = get_run_nr(root_dir=log_root_dir, comment=comment, last=use_existing)

    if use_existing and (log_number != model_number):
        raise Exception(f"Somethings not right with the run numbers.\nmodel_number: {model_number}, log_number: {log_number}")
    if not comment and (mdl_cmt != log_cmt):
        raise Exception(f"Somethings not right with the run comments.\nmodel comment: {mdl_cmt}, log comment: {log_cmt}")

    if not comment: 
        comment = mdl_cmt
    log_dir = os.path.join(log_root_dir, str(log_number) + "_" + comment)
    model_dir = os.path.join(model_root_dir, str(model_number) + "_" + comment)
    return log_dir, model_dir


def print_configuration(args, level=0):
    """ Generates a string representation for a given dict of arguments,
    handling nested dictionaries and lists. """
    conf_str = "" if level == 0 else "\n"
    for k,v in args.items():
        conf_str += "\t"*level+f"{k} : "
        if type(v) == dict:
            conf_str += print_configuration(v, level=level+1)
        elif type(v) == list:  # I here just assume that there are no dicts in lists
            if len(v) < 10:
                conf_str += str(v)
            else:
                conf_str += "[ "
                first = True
                for i, e in enumerate(v):
                    conf_str += "" if first else ", "
                    first = False
                    conf_str += str(e)
                    if i == len(v)-1: break
                    if (i+1) % 5 == 0:
                        conf_str += ",\n" + "\t"*(level+2) # two tabs because key of list
                        first = True
                conf_str += " ]"
        else:
            if type(v) == bool:
                v = str(v).lower()
            conf_str += f"{v}"
        conf_str += "\n"  # TODO: some newline issues
    return conf_str
 

# LOAD MODEL STUFF
def get_files_sorted_by_mtime(directory):
    """Return files sorted by modification time."""
    files = glob.glob(f"{directory}/**/*.pth", recursive=True)
    return sorted(files, key=os.path.getmtime)

def get_files_of_type(files, type):
    """Return files of a specific type."""
    return [f for f in files if re.search(f".*/{type}\.pth", f)]

def get_last_model_names(model_dir): 
    sorted_files = get_files_sorted_by_mtime(model_dir)

    # Filter by type
    g_PT_files = get_files_of_type(sorted_files, "G_PT")
    d_PT_files = get_files_of_type(sorted_files, "D_PT")
    g_FT_files = get_files_of_type(sorted_files, "G_FT")
    d_FT_files = get_files_of_type(sorted_files, "D_FT")
    g_files = get_files_of_type(sorted_files, "G")
    d_files = get_files_of_type(sorted_files, "D")
    g_files = [f for f in g_files if f not in g_PT_files+g_FT_files]
    d_files = [f for f in d_files if f not in d_PT_files+d_FT_files]

    # Collect the most recent files of each type
    g_last = [max(g_files+g_PT_files+g_FT_files, key=os.path.getmtime, default=None)]
    d_last = [max(d_files+d_PT_files+d_FT_files, key=os.path.getmtime, default=None)]

    return {
        "g": g_files[-1:],
        "d": d_files[-1:],
        "g_PT": g_PT_files[-1:],
        "d_PT": d_PT_files[-1:],
        "g_FT": g_FT_files[-1:],
        "d_FT": d_FT_files[-1:],
        "g_last": g_last if g_last else [],
        "d_last": d_last if d_last else [],
    }, sorted_files


def get_load(load_arg, model_dir):
    if model_dir is None:
        raise Exception("need to specify model-directory if you want to load")
    trained_last, model_list = get_last_model_names(model_dir)  # Names of the most recently trained (saved) models
    if len(model_list) == 0:
        raise Exception(f"Can't load: no models in {model_dir}")

    load_dict = {}
    for m in load_arg:  # m model(-key) can be like "g" or "d_FT" or "g_PT" or "path/to/G.pth"
        if os.path.exists(m):
            model = m.split("/")[-1][0]
            key = model.lower()
            if key not in ["g", "d"]:
                raise Exception(f"Your model path cannot be assigned to a model: {m}. " + \
                                 "It should match 'G.*\\.pth' or 'D.*\\.pth'.")
        elif m in trained_last: # Case: Label of model to load (like g_FT, d_PT, g)
            tag = m
            key = tag[0]
            m = trained_last[tag]
            if len(m) == 0:
                raise Exception(f"No model for {tag}")
            m = m[0]
        else: 
            raise Exception(f"Invalid argument: {m}")

        load_dict[key] = m

    return load_dict

 


def str2bool(val):
    """ Converts a string representation of bool to True or False.
    True values  : 'y', 'yes', 't', 'true',   'on', '1' 
    False values : 'n',  'no', 'f', 'false', 'off', '0'
    All values can also be in capitals.
    If 'val' is anything else: ValueError thrown.
    """
    val = val.lower()
    if val in ('y', 'yes', 't', 'true', 'on', '1'):
        return True  # return 1
    elif val in ('n', 'no', 'f', 'false', 'off', '0'):
        return False # return 0
    else:
        raise ValueError(f"Invalid boolean value {val}")



# DEBUGGING

def print_attrs(obj):
    vs = [a for a in dir(obj) if not a.startswith("__") and not callable(getattr(obj, a))]
    for v in vs:
        print(f"{v}:")
        print(getattr(obj, v))
        print("--------------------------------------------------")


def print_obj_attrs(obj, level=0):
    print(f"------------------l:{level}--------------------")

    if level > 3: 
        print(type(obj))
        return
    atrs = [a for a in dir(obj) if (not a.startswith('__') and not callable(getattr(obj,a)))]
    print(f"num of attrs: {len(atrs)}")
    for attr in dir(obj):
        if attr.startswith("__"):
            continue
        val = getattr(obj, attr)
        if callable(val):
            continue

        # Name of attr
        print("\t"*level + str(attr) + " : ", end ="")

        #print(attr, type(attr))
        if isinstance(val, dict):
            #print(".. is dict")
            print(print_configuration(val, level=level+1))
        elif type(val) == list:
            #print(".. is list")
            print(print_configuration({0: val}, level=level+1))
        elif isinstance(val, torch.nn.Module):
            #print(".. is torch module")
            print(print_obj_attrs(val, level=level+1))
        elif type(str(val)) is str and len(str(val)) > 100:
            #print(".. is none of those AND LONG")
            print(str(type(val)), end="")
            if hasattr(val, "shape"):
                 print(", " + str(val.shape))
            else: 
                print()
        else:
            #print(".. is none of those AND SHORT")
            print(val)

