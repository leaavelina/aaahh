#!/usr/bin/env python

import time
import os
import datetime
import warnings
import pathlib
import math
import ast

import numpy as np
np.set_printoptions(suppress=True)
import pandas as pd

import librosa
import opensmile
import audiofile
import parselmouth
from parselmouth.praat import call
from matplotlib import pyplot as plt
from utils import timestr#, print_attrs


# openSMILE configuration (using V2)
EGEMAPS_V2_CONFIG = os.path.join(os.path.abspath(os.path.dirname(__file__)), "os_config/egemaps/v02/eGeMAPSv02.conf")
EGEMAPS_V1_CONFIG = os.path.join(os.path.abspath(os.path.dirname(__file__)), "os_config/egemaps/v01b/eGeMAPSv01b.conf")


# needed for emotion to vector
emo_enum = {
    "other": 0,
    "anger": 1,
    "frustration": 2,
    "happiness": 3,
    "sadness": 4,
    "neutral": 5,
}


# Extract and prepare features for the dataset
class FeatureExtractor:
    def __init__(self, seq_len=71, marker_set=None, egemaps_set=None, t_step=0.0083125, t_window=0.025, max_nan_seq=10, n_mfcc=25, **params):
        ''' Init with feature parameter configuration. '''
        self.min_frames = seq_len
        self.marker_set = [m.lower() for m in marker_set] if marker_set is not None else []
        self.egemaps_set = [e for e in egemaps_set] if egemaps_set is not None else []
        self.t_step = t_step
        self.t_window = t_window
        self.max_nan_seq = max_nan_seq
        self.n_mfcc = n_mfcc
        self.seq_len = seq_len


    def marker_features(self, file:str, frame:bool=True, extracted:bool=False):
        ''' Returns marker-features extracted from a file, as a pandas DataFrame or a NumPy array.
        Handles marker subset selection, padding of short sequences, and interpolates missing data. '''
        if extracted:
            features = pd.read_csv(file, index_col=0)
        else:
            with open(file, "r") as f:
                mrk_names = next(f)[:-1].split(" ")

            col_names = [mrk_names.pop(0), mrk_names.pop(0).lower()]  # Frame# , time
            for i, mrk in enumerate(mrk_names):
                mrk = mrk.lower()
                col_names.extend([mrk+"_x", mrk+"_y", mrk+"_z"])

            features = pd.read_csv(file, sep=" ", skiprows=[0, 1], header=None, names=col_names, index_col="time").drop('Frame#', axis=1)
            if len(self.marker_set) > 0:
                ms = []
                for m in self.marker_set:
                    ms.extend([m.lower() + "_x", m.lower() + "_y", m.lower() + "_z"])
                features = features[ms]  # Ordered like self.marker_set, for vizualization


            n = max(math.ceil(features.index[-1] / self.t_step), self.seq_len)
            while len(features) < n:  # Ses03M_impro07_M007 is the only ocurrence (length 70)
                # pad with values of last frame (index doesnt matter as it will be changed)
                features.loc[features.index[-1]+self.t_step] = features.iloc[-1]   

            # time indices lying inside the audio indices (start, stop)
            index = [round(self.t_step*i+(self.t_step/2), 10) for i in range(n)]
            # rounding to 10th decimal because of floating point error
            filler_rows = pd.DataFrame(np.nan, index=index, columns=features.columns)
            features = pd.concat((features, filler_rows)).sort_index()

            # NOTE: interpolation doesn't get the row first or last row if its nan so i fill it with adjecent values
            if features.iloc[0].isna().any().any(): 
                features.loc[index[0]] = features.iloc[1]
            if features.iloc[-1].isna().any().any():
                features.loc[index[-1]] = features.iloc[-2]

            features = features.interpolate(method='polynomial', order=3, limit_direction='both', limit=self.max_nan_seq)
            features = features.loc[index]
            features.index.name = "time"


        if len(features) < self.min_frames:
            raise ValueError(f"Number of frames too small: {len(features)} < {self.min_frames}")


        features = features.astype('float32')
        if not frame:
            return features.to_numpy()
        return features


    def audio_features(self, file, start=0.0, end=None, frame=True, extracted=False):
        ''' Extracts audio features from a given file (audio file, or pre-extracted), 
        with optional start and end times. Handles necessary padding and overlap adjustments.
        Returns pandas DataFrame or numpy array, depending on `frame`. '''
        def adjust_time(time):
            time = math.floor(time/self.t_step)*self.t_step
            return round(time, 10) # for float precision errors

        if extracted:
            features = pd.read_csv(file, index_col=[0,1], dtype={'start':float, 'end':float})
            start = adjust_time(start)
            end = adjust_time(end)

            # Get indices
            indices = features.reset_index()[["start", "end"]].values
            indices = np.around(indices, decimals=10)
            index_start = np.argwhere(indices[:,0] == start)  # looks now like this: [[idx]]
            index_end = np.argwhere(indices[:,0] == end)
            if len(index_start) == 0:
                raise ValueError(f"Extracted feature file {file}: startindices empty.")
            if len(index_end) == 0:
                raise ValueError(f"Extracted feature file {file}: startindices empty.")
            index_start = index_start.reshape((1))[0]
            index_end = index_end.reshape((1))[0]

            features = features.iloc[index_start: index_end+1]

        else:
            signal, sr = audiofile.read(file)
            duration = len(signal)/sr
            end = duration if end is None else end
            end += 0.06  # for eGeMAPS (second window len is 0.06)
            pad_end = max(0, end-duration)
            pad_end = int(pad_end*sr)
            signal = np.append(signal, [0]*pad_end)  # 0 means silence
            if start > 0.0:
                raise(NotImplementedError("Sorry, but you can only specify 'end' for now."))

            start_frame = int(start / self.t_step)  # time index divisible by t_step

            # Feature calculation calls
            mfccs = self.spectral_features(signal, sr)
            features = self.emotional_speech_features(signal, sr, file)

            if mfccs.shape[-1] < self.min_frames or len(features) < self.min_frames:
                raise ValueError(f"Either MFCCs or EGEMAPS have not enough frames (file: {file})")

            maxlen = min(len(features), mfccs.shape[-1])
            features = features[:maxlen]
            mfccs = mfccs[:,:maxlen]

            if frame:
                index = [(round(fi*self.t_step, 10), round(fi*self.t_step + self.t_window, 10))
                            for fi in range(start_frame, start_frame+len(features))]
                features.index = pd.MultiIndex.from_tuples(index, names=["start", "end"])
                for i in range(len(mfccs)):
                    features = features.assign(mfcc_temp=mfccs[i]).rename(columns={"mfcc_temp": f"mfcc_{i+1}"})
            else:  # if not (pandas data-)frame: index and column names irrelevant
                features = features.to_numpy()
                features = np.hstack((features, np.transpose(mfccs)))

        features = features.astype('float32')
        return features

        # If the signal is 0 all features are 0 except for columns:
        # ["Loudness_sma3", "slope0-500_sma3", "slope500-1500_sma3", "F1amplitudeLogRelF0_sma3nz", "F2amplitudeLogRelF0_sma3nz", "F3amplitudeLogRelF0_sma3nz"]
        # and these are their values respectively: [0.00103397, 0.11200001, 0.07636364, -201., -201., -201. ]


    def emotional_speech_features(self, signal, sr, file=None):  
        ''' Calculates speech features from audio signal. 
        20 features in total (18 eGeMAPS, F0, Intensity).
        Using openSMILE for eGeMAPS and Parselmouth for Intensity and F0. '''
        # eGeMAPS features
        smile = opensmile.Smile(
            feature_set=EGEMAPS_V2_CONFIG, #opensmile.FeatureSet.eGeMAPSv02,
            feature_level='lld',           #opensmile.FeatureLevel.LowLevelDescriptors,
        )
        egemaps = smile.process_signal(signal, sr)
        if len(self.egemaps_set) > 0:
            cols = [c for c in egemaps.columns if c in self.egemaps_set]
            egemaps = egemaps[cols]

        # Intensity
        sound = parselmouth.Sound(file)
        duration = sound.tmax
        intensity = call(sound, "To Intensity", 150, self.t_step).values[0]  # minimumPitch, time_step
        pad = max(len(egemaps) - len(intensity), 0)

        intensity = np.concatenate((intensity, np.tile(intensity[-1], pad+2)))
        # Average every three values: window_size of 0.0083125 * 3 = 0.0249375 ~ 0.025
        intensity = np.array([np.mean(intensity[i:i+3]) for i in range(len(intensity)-2)])
        
        # F0
        pitch = call(sound, "To Pitch", self.t_step, 50, 300)  # time_step, f0min, f0max
        pitch = np.array([[freq, stren] for freq, stren in pitch.selected_array])
        pad = len(egemaps)-len(pitch) if len(egemaps) > len(pitch) else 0
        pad = max(len(egemaps) - len(pitch), 0)

        pitch = np.concatenate((pitch, np.tile(pitch[-1], (pad, 1))))

        # Pad to match egemaps length if necessary (up to 1-2 frames)
        egemaps["F0"] = pitch[:len(egemaps), 0]
        egemaps["Intensity"] = intensity[:len(egemaps)]

        return egemaps

    def spectral_features(self, signal, sr:float):
        ''' Extracts MFCCs from audio signal, using Librosa. '''
        hop_length = int(sr*self.t_step)
        window_length = int(sr*self.t_window)
        mfccs = librosa.feature.mfcc(y=signal, sr=sr, n_mfcc=self.n_mfcc,
                                     hop_length=hop_length, win_length=window_length,
                                     n_fft=window_length, center=False)
        return mfccs


    @staticmethod
    def emotion_features(ef, extracted=False):
        ''' Processes emotion features.
        Input data (ef) can be a dictionary or file path. 
        Returns a normalized emotion vector. '''
        if extracted:
            with open(ef, 'r') as f:
                # in extracted files the emo dict is saved as:
                # ("name": turn_name, "emo": emo_str, "time", time_range)   
                # where emo_str is like "Angry;Angry;Neutral"
                s = ""
                s = ''.join(line.strip() for line in f.readlines())
                ef = ast.literal_eval(s)

        emo_str = ef["emos"]
        emo_list = [e.strip().lower() for e in emo_str.split(";")]
        emo_vec = np.zeros(shape=(6,))
        for e in emo_list:
            emo_vec[emo_enum.get(e, 0)] += 1
        emo_vec /= len(emo_list)

        return emo_vec.astype('float32')
        
    # Note: FeatureExtractor should be initialized with the params for the feature-calculation
    def features_to_files(self, file_paths, target_dir="features/", override=False, feature_types='all'):
        ''' Exports computed audio and/or marker and/or emotion features to files into specified directory. '''
        print("Writing features to files...")
        # file_paths is tuple of three lists of strings of the paths
        audio_fp, marker_fp, emo_data = file_paths

        if type(feature_types) not in [list, tuple]:
            feature_types = ['marker', 'emo', 'audio'] if feature_types == 'all' else [feature_types]

        os.makedirs(target_dir, exist_ok=True)

        start_time = time.time()
        for i, audio_f in enumerate(audio_fp):
            turn_name = audio_f.split("/")[-1].split(".")[0]
            #turn_name = os.path.splitext(os.path.basename(audio_f))[0]

            if 'emo' in feature_types:
                e_target = os.path.join(target_dir,turn_name+".ef.txt")
                if override or not os.path.exists(e_target):
                    with open(e_target, 'w') as f:
                        f.write(str(emo_data[i]))  # dict to string

            if 'marker' in feature_types:
                m_target = os.path.join(target_dir,turn_name+".mf.csv")
                if override or not os.path.exists(m_target):
                    mf = self.marker_features(marker_fp[i])
                    mf.to_csv(m_target, header=True, index=True)

            if 'audio' in feature_types:
                a_target = os.path.join(target_dir,turn_name+".af.csv")
                if override or not os.path.exists(a_target):
                    af = self.audio_features(file=audio_f)
                    af.to_csv(a_target)

            print(f"\r{i+1}/{len(audio_fp)}  ({100*(i+1)/len(audio_fp):.0f}%)\t", end="")
        print(f"\nProcess completed in {timestr(int(time.time() - start_time), 'sentence')}")



'''
egemaps_set = [
    # Frequency related Params:
    F0semitoneFrom27.5Hz_sma3nz,
    jitterLocal_sma3nz,
    F1frequency_sma3nz,
    F3frequency_sma3nz,
    F2frequency_sma3nz,
    F1bandwidth_sma3nz,
    # Energy / Amplitude related parameters:
    shimmerLocaldB_sma3nz,
    Loudness_sma3,
    HNRdBACF_sma3nz,
    # Spectral (balance) parameters:
    alphaRatio_sma3,
    hammarbergIndex_sma3,
    slope0-500_sma3,
    slope500-1500_sma3,
    F1amplitudeLogRelF0_sma3nz,
    F2amplitudeLogRelF0_sma3nz,
    F3amplitudeLogRelF0_sma3nz,
    logRelF0-H1-H2_sma3nz,
    logRelF0-H1-A3_sma3nz
]'''


