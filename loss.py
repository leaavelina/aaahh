import torch
from math import prod

def pearson_correlation(x, y):
    xy = (x - x.mean()) * (y - y.mean())
    return xy.sum() / (torch.sum(x**2) * torch.sum(y**2)).sqrt()

def ccc_loss(output, target):
    ro = pearson_correlation(output, target)
    ccc = (2*ro * output.std() * target.std()) / (output.var() + target.var() + (output.mean() - target.mean())**2)
    return 1 - ccc


