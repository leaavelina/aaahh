#!/usr/bin/env python

import time
from datetime import timedelta
import random
import glob
import os
import ruamel.yaml as yaml
import inspect
from typing import get_type_hints

import argparse
from argparse import Namespace
import pathlib
import sys

import numpy as np
import torch
from torch.utils.tensorboard import SummaryWriter

from utils import timestr, get_run_dirs, str2bool
from utils import get_last_model_names, print_configuration, get_load
from dataset import get_dataloader
from csg import Generator, Discriminator, init_weights
from loss import ccc_loss
from trainer import Trainer, Logger
from viz import plot_history

from utils import print_obj_attrs


def main(args):
    if args.seed:
        random.seed(args.seed)
        np.random.seed(args.seed)
        torch.manual_seed(args.seed)

    # Logdir & Logger
    log_dir, model_dir = get_run_dirs(log_root_dir=args.log_dir, model_root_dir=args.model_dir, 
                                      comment=args.comment, use_existing=args.overwrite)
    args.log_dir = log_dir
    args.model_dir = model_dir

    logger = Logger(verbose=args.verbose, log_dir=args.log_dir)
    logger.log(timestr(time.time(), 'date'))

    if args.gpu:
        if torch.cuda.is_available():
            device = torch.device("cuda")
        elif torch.backends.mps.is_available():
            device = torch.device("mps")  # Mac (M1) GPU
        else:
            logger.log("No gpu available. Using cpu")
            device = torch.device("cpu")
    else:
        device = torch.device("cpu")

    # Tensorboard
    if args.tensorboard:
        tb_dir = os.path.join("runs/", log_dir.split("/")[-1])  # tensorboard log dir always called "runs" like default
    else:
        tb_dir = None

    # Feature Params and Model Params
    feature_params = args.feature_params

    if feature_params.get("marker_set") is None:
        raise Exception("The marker set must be given")
    if feature_params.get("egemaps_set") is None:
        raise Exception("The eGeMAPS set must be given")
    n_mf = len(feature_params["marker_set"]) * 3
    n_af = len(feature_params["egemaps_set"]) + feature_params["n_mfccs"] + 2  # +2 for F0 and intensity
    n_e = 6 
    noise_dim = feature_params["noise_dim"]

    g_input_size = n_af + n_e + noise_dim
    d_input_size = n_mf + n_af + n_e

    model_params = args.model_params
    model_params["g_input_size"] = g_input_size
    model_params["g_output_size"] = n_mf
    model_params["d_input_size"] = d_input_size

    # Log configuration
    conf_str = "Configuration of this run:\n" + print_configuration(vars(args)) + "\n"
    logger.log(conf_str, v=1)

    # Remove attributes after logging them
    del args.seed
    del args.feature_params
    del args.model_params
    args.tb_dir = tb_dir

    # DataLoader for Train- and Test-set
    dataset_params = args.dataset_params

    if not args.gpu:
        args.dataset_params["num_workers"] = 0
    dl = get_dataloader(args.dataset_root, **dataset_params, **feature_params)
    test_dl = get_dataloader(args.dataset_root, **dataset_params, **feature_params, train=False)
    del args.dataset_params
    del args.gpu

    # class from string -> has to be a torch optimizer
    optim_params = args.optim_params
    optim_cls = getattr(torch.optim, optim_params["optim"])
    optim_params["optim"] = optim_cls
    del args.optim_params

    # To not pass unnecessary attributes to the Train initialization
    train_configuration = args.train
    del args.train
    load = args.load
    del args.load

    # Create the trainer
    trainer = Trainer(device=device, logger=logger, **vars(args), noise_dim=noise_dim)
    trainer.init_model(**model_params)

    # Load models if specified
    if load:
        trainer.load_model(g_path=load.get("g"), d_path=load.get("d"))  # if g or d are not specified they are None

    trainer.init_optimizers(**optim_params)

    if args.plot:
        os.makedirs(os.path.join(log_dir, "plots/"), exist_ok=True)

    # Training
    g_ep = train_configuration["g_epochs"]
    if g_ep > 0:
        logger.log("\nPretraining Generator")
        history = trainer.train(epochs=g_ep, data=dl, test_data=test_dl, pretrain="G")
        if args.plot:
            plot_history(history.to_dataframe(), target=os.path.join(f"{log_dir}/plots/history_G.jpg"))

    d_ep = train_configuration["d_epochs"]
    if d_ep > 0:
        logger.log("\nPretraining Discriminator")
        history = trainer.train(epochs=d_ep, data=dl, test_data=test_dl, pretrain="D")
        if args.plot:
            plot_history(history.to_dataframe(), target=os.path.join(f"{log_dir}/plots/history_D.jpg"))

    ep = train_configuration["epochs"]
    if ep > 0:
        logger.log("\nTraining GAN")
        history = trainer.train(epochs=ep, data=dl, test_data=test_dl)
        if args.plot:
            plot_history(history.to_dataframe(), target=os.path.join(f"{log_dir}/plots/history.jpg"))

    fep = train_configuration["finetune_epochs"]
    if fep > 0:
        logger.log("\nFinetuning GAN")
        history = trainer.train(epochs=fep, data=dl, test_data=test_dl, finetune=True)
        if args.plot:
            plot_history(history.to_dataframe(), target=os.path.join(f"{log_dir}/plots/history_FT.jpg"))

    


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-c", "--comment", required=False, 
                        help="title for this run")
    parser.add_argument("-s", "--seed", required=False, type=int,
                        help="seed")
    parser.add_argument("-g", "--gpu", required=False, action="store_true", 
                        help="enable gpu usage")
    parser.add_argument("-x", "--extracted", default=None, const=True, type=str2bool, nargs="?",
                        help="use the extracted features instead of the original dataset")
    parser.add_argument("-p", "--plot", required=False, action="store_true",
                        help="plot the train history after finish")
    parser.add_argument("-t", "--tensorboard", default=None, const=True, type=str2bool, nargs="?", metavar="Y/N", 
                        help="use tensorboard")
    parser.add_argument("-i", "--validation-interval", default=5, type=int, metavar="NUM",
                        help="Interval for validation run.")
    parser.add_argument("-v", "--verbose", default=1, required=False, type=int, metavar="NUM",
                        help="0: no output, 1: config & progress prints")
    parser.add_argument("--config_file", default="params.yaml", 
                        help="file (yaml or json) for all parameters")

    # Loading
    parser.add_argument("-l", "--load", dest="load", action="append", required=False, metavar="MODEL",
                        help="load models: -l <modelname> OR -l g|d[_<tag>] for most recently" +\
                             "finetuned (tag=FT), pretrained (tag=PT), trained (no tag)")
    parser.add_argument("-o", "--overwrite", required=False, action="store_true",
                        help="overwrite existing history.csv files and models (of a run with the same comment)")

    # Paths
    parser.add_argument("--dataset-root", required=False, metavar="PATH",
                        help="path to dataset, can be the original dataset or the extracted features")
    parser.add_argument("--log-dir", required=False, metavar="PATH",
                        help="path for logs, history and plots")
    parser.add_argument("--model-dir", required=False, metavar="PATH",
                        help="directory for saving / loading models")
    parser.add_argument("--checkpoint-dir", required=False, metavar="PATH",
                        help="directory for saving / loading checkpoints")

    # Checkpoints
    parser.add_argument("--checkpoints", default=False, action="store_true",
                        help="enable checkpoints")
    parser.add_argument("--checkpoint-interval", required=False, type=int, metavar="NUM",
                        help="epoch-interval for saving checkpoints")

    parser.add_argument("-e", "--emotions", default=None, const=True, type=str2bool, nargs="?",
                        help="use emotion features")
    parser.add_argument("-n", "--noise_dim", required=False,
                        help="n noise features")

    # Params for UNTERKATEGORIEN TODO englisch 
    parser.add_argument("--feature-params", default="{}", type=str,
                        help="string for feature parameters (yaml or json)")
    parser.add_argument("--model-params", default="{}", type=str,
                        help="string for model parameters (yaml or json)")
    parser.add_argument("--optim-params", default="{}", type=str,
                        help="string for optimizer parameters (yaml or json)")
    parser.add_argument("--dataset-params", default="{}", type=str,
                        help="string for dataset parameters (yaml or json)")
    #parser.add_argument("--preprocess-params", default="{}", type=str,
    #                    help="string for preprocessing parameters (yaml or json)")

    # Train run configuration
    parser.add_argument("-gep", "--g-epochs", required=False, help="number of epochs of generator pretraining")
    parser.add_argument("-dep", "--d-epochs", required=False, help="number of epochs of discriminator pretraining")
    parser.add_argument("-ep", "--epochs", required=False, help="number of epochs of adversarial training")
    parser.add_argument("-fep", "--finetune-epochs", required=False, help="number of epochs of GAN finetuning")


    args_ , remaining = parser.parse_known_args()

    with open(args_.config_file, "r") as f:
        params_ = yaml.safe_load(f)
    del args_.config_file

    # Directories
    if not args_.dataset_root: # args_.dataset_root can be a path to an extracted dataset, as long as --extracted
        if args_.extracted:
            args_.dataset_root = params_["extracted_dataset_root"]
        else: 
            args_.dataset_root = params_["dataset_root"]

    if not args_.log_dir:
        args_.log_dir = params_["log_dir"]
    if not args_.model_dir:
        args_.model_dir = params_["model_dir"]
    if not args_.checkpoint_dir:
        args_.checkpoint_dir = params_["checkpoint_dir"]

    if args_.checkpoint_interval is None:
        if args_.checkpoints:
            args_.checkpoint_interval = params_["checkpoints"]
        else:
            args_.checkpoint_interval = 0  # checkpoints off

    if args_.comment is None:
        args_.comment = params_["comment"]

    # Parameters for the different parameter-dicts
    feature_params_ = params_["feature_params"]
    feature_params_.update(yaml.safe_load(args_.feature_params))
    if args_.noise_dim:
        feature_params_["noise_dim"] = args_.noise_dim
    args_.feature_params = feature_params_

    del args_.noise_dim

    model_params_ = params_["model_params"]
    model_params_.update(yaml.safe_load(args_.model_params))
    args_.model_params = model_params_

    optim_params_ = params_["optim_params"]
    optim_params_.update(yaml.safe_load(args_.optim_params))
    args_.optim_params = optim_params_
    
    dataset_params_ = params_["dataset_params"]
    dataset_params_.update(yaml.safe_load(args_.dataset_params))
    if args_.extracted:
        dataset_params_["extracted"] = args_.extracted
    if args_.emotions:
        dataset_params_["emotions"] = args_.emotions
    args_.dataset_params = dataset_params_

    del args_.extracted
    del args_.emotions
    

    # Train run configuration
    train = {}
    for k, v in params_["train"].items():
        if not hasattr(args_, k):
            continue
        v_ = getattr(args_, k)
        train[k] = v if v_ is None else int(v_)
        delattr(args_, k)
    args_.train = train

    # Loading
    if args_.load is not None:  # args_.load is a list of strings
        args_.load = get_load(args_.load, args_.model_dir)  # this is the root_model_dir, not the one with the run-number & comment.
    else:
        args_.load = False
    
    main(args_)


