#!/usr/bin/env python

import time
from datetime import timedelta
import random
import glob
import os
import ruamel.yaml as yaml
import inspect
from typing import get_type_hints

import argparse
from argparse import Namespace
import pathlib
import sys

import numpy as np
import torch
from torch.utils.tensorboard import SummaryWriter

from utils import timestr, get_run_dirs, str2bool
from utils import get_last_model_names, print_configuration

from dataset import get_dataloader
from csg import Generator, Discriminator, init_weights
from loss import ccc_loss
from trainer import Trainer, Logger
from viz import plot_history, plot_g_val, plot_d_val, selection1, selection2, g_all_plots, all_plots

#debugging
from utils import print_obj_attrs, get_load

def main(args):
    # Logger & Logdir
    log_dir, model_dir = get_run_dirs(log_root_dir=args.log_dir, model_root_dir=args.model_dir, 
                                      comment=args.comment, use_existing=True)
    args.log_dir = log_dir
    args.model_dir = model_dir  # this model_dir is the subdirectory beloning to a run.
    logger = Logger(verbose=args.verbose, log_dir=args.log_dir, training=False)
    logger.log(timestr(time.time(), 'date'))

    if args.gpu:
        if torch.cuda.is_available():
            device = torch.device("cuda")
        elif torch.backends.mps.is_available():
            device = torch.device("mps")  # Mac M1 GPU
        else:
            logger.log("No gpu available. Using cpu")
            device = torch.device("cpu")
    else:
        device = torch.device("cpu")

    # Feature Params and Model Params
    feature_params = args.feature_params

    if feature_params.get("marker_set") is None:
        raise Exception("The marker set must be given")
    if feature_params.get("egemaps_set") is None:
        raise Exception("The eGeMAPS set must be given")
    n_mf = len(feature_params["marker_set"]) * 3
    n_af = len(feature_params["egemaps_set"]) + feature_params["n_mfccs"] + 2  # 2 for F0 and intensity
    n_e = 6
    noise_dim = feature_params["noise_dim"]

    g_input_size = n_af + n_e + noise_dim
    d_input_size = n_mf + n_af + n_e

    model_params = args.model_params
    model_params["g_input_size"] = g_input_size
    model_params["g_output_size"] = n_mf
    model_params["d_input_size"] = d_input_size

    # Log configuration
    val_config = {"config_file": args.config_file, "comment": args.comment, "log_dir": args.log_dir,
                  "gpu": args.gpu, "extracted": args.dataset_params["extracted"], "load": args.load, "show_plot": args.show_plot}
    conf_str = "Validation Configuration:\n" + print_configuration(val_config) + "\n"
    logger.log(conf_str, v=1)

    # Remove attributes after logging them
    del args.feature_params
    del args.model_params

    # DataLoader for Test-set
    dataset_params = args.dataset_params
    if not args.gpu:
        args.dataset_params["num_workers"] = 0
    dl = get_dataloader(args.dataset_root, **dataset_params, **feature_params, train=False)
    del args.dataset_params
    del args.gpu

    load = args.load
    del args.load

    # Create the trainer
    trainer = Trainer(device=device, **vars(args), checkpoint_dir=None, 
                      checkpoint_interval=0, validation_interval=0, logger=logger, noise_dim=noise_dim)
    trainer.init_model(**model_params)

    # load is never None
    trainer.load_model(g_path=load.get("g"), d_path=load.get("d"))  # if g or d are not specified they are None

    # Validate the models on the test dataset (Session 5)
    history = trainer.test(data=dl, training=False)
    
    # Plot the history
    #plot_g_val(history, *g_all_plots); exit()
    #plot_d_val(history, *all_plots, path=log_dir)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # arguments def
    parser.add_argument("-c", "--comment", default=None, required=False, 
                        help="title of the run to be validated")
    parser.add_argument("--show-plot", default=None, const=True, type=str2bool, nargs="?",
                        help="show the plot.")

    parser.add_argument("-g", "--gpu", required=False, action="store_true", 
                        help="enable gpu usage")
    parser.add_argument("-v", "--verbose", "-v", default=1, required=False, type=int,
                        help="amount of output: 0: no output, 1: progress, loss and prediction.")

    parser.add_argument("--config_file", default="params.yaml", required=False,
                        help="file (yaml or json) for all parameters")

    parser.add_argument("-l", "--load", dest="load", action="append", required=False, 
                        help="load models: -l <modelname> OR -l g|d[_<tag>] for most recently" +\
                             "finetuned (tag=FT), pretrained (tag=PT), trained (no tag)")

    parser.add_argument("--dataset-root", required=False, 
                        help="path to dataset")
    parser.add_argument("--log-dir", required=False,
                        help="path to history-logs")
    parser.add_argument("--model-dir", required=False,
                        help="directory with models")

    parser.add_argument("-x", "--extracted", default=None, const=True, type=str2bool, nargs="?",
                        help="use the extracted features instead of the original dataset")

    parser.add_argument("-e", "--emotions", default=None, const=True, type=str2bool, nargs="?",
                        help="use emotion features")

    parser.add_argument("-n", "--noise_dim", required=False,
                        help="n noise features")

    parser.add_argument("--feature-params", default="{}", type=str,
                        help="string for feature parameters (yaml or json)")
    parser.add_argument("--model-params", default="{}", type=str,
                        help="string for model parameters (yaml or json)")
    parser.add_argument("--dataset-params", default="{}", type=str,
                        help="string for dataset parameters (yaml or json)")

    # end arguments def # 

    args_ , remaining = parser.parse_known_args()

    with open(args_.config_file, "r") as f:
        params_ = yaml.safe_load(f)
    #del args_.config_file

    # Directories
    if not args_.dataset_root: # args_.dataset_root can be a path to an extracted dataset, as long as --extracted
        if args_.extracted:
            args_.dataset_root = params_["extracted_dataset_root"]
        else: 
            args_.dataset_root = params_["dataset_root"]
    if not args_.log_dir:
        args_.log_dir = params_["log_dir"]
    if not args_.model_dir:
        args_.model_dir = params_["model_dir"]

    #if args_.comment is None: # THIS IS NOT NEEDED IN VALIDATION
    #    args_.comment = params_["comment"]

    # Parameters for the different sub-params-dicts
    feature_params_ = params_["feature_params"]
    feature_params_.update(yaml.safe_load(args_.feature_params))
    if args_.noise_dim:
        feature_params_["noise_dim"] = args_.noise_dim
    args_.feature_params = feature_params_
    del args_.noise_dim

    model_params_ = params_["model_params"]
    model_params_.update(yaml.safe_load(args_.model_params))
    args_.model_params = model_params_

    dataset_params_ = params_["dataset_params"]
    dataset_params_.update(yaml.safe_load(args_.dataset_params))
    if args_.extracted is not None:
        dataset_params_["extracted"] = args_.extracted
    if args_.emotions is not None:
        dataset_params_["emotions"] = args_.emotions
    args_.dataset_params = dataset_params_
    del args_.extracted
    del args_.emotions
    

    # Loading
    if args_.load is None:  # args_.load is a list of strings
        print("Models to load were not specified. Using most recently saved.")
        args_.load = ['g_last', 'd_last']
    args_.load = get_load(args_.load, args_.model_dir)  # this model_dir is the root model dir

    main(args_)

