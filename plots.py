import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc, precision_recall_curve
from sklearn.metrics import confusion_matrix as conf_matrix


def boxplot(data, titles, y_label=""): # KATEGORIE VERTEILUNGEN
    """ Boxplot. Next to each other. """
    n_subplots = len(data)
    
    flierprops = dict(marker="_", alpha=0.5)
    fig, axs = plt.subplots(1, n_subplots, figsize=(3*n_subplots, 7), sharey=True)
    
    for i in range(n_subplots):
        axs[i].boxplot(data[i], flierprops=flierprops) 
        if i == 0 and y_label:
            axs[i].set_ylabel(y_label)
        axs[i].set_title(titles[i])
        axs[i].set_xticks([])
    
    plt.tight_layout()
    return fig, axs


def heatmap(data, titles, colnames, cbar_label="", suptitle=""):
    n_rows = len(titles)  # len(titles)
    n_cols = len(colnames)
    fig, axs = plt.subplots(n_rows, n_cols, figsize=(7*n_cols, 3*n_rows))
    if n_rows == 1: # case only various columns and 1 row.
        axs = [axs]
        data = np.array([data])
        axes = list(range(len(data.shape)))
        axes.append(axes.pop(1))
        data = np.transpose(data, axes)

    im = None
    for i in range(n_rows):
        title_height = 1-((i+1)*0.25)
        fig.text(0.05, title_height, titles[i], va="center", rotation=90, size=14)
        for j in range(n_cols):
            im = axs[i][j].imshow(data[i][...,j].T, cmap="hot", aspect="auto")
            if j == 0: # Only the leftmost have labeled y-axes.
                axs[i][j].set_ylabel("Sequence")
            else:
                axs[i][j].set_xticks([])
            if i == n_rows-1:  # Only the bottom ones have labeled x-axes.
                axs[i][j].set_xlabel("Samples")
            else:
                axs[i][j].set_xticks([])
            
    for i in range(n_cols):
        fig.text(0.32*(i+1), 0.92, colnames[i], ha="center", size=14)

    fig.subplots_adjust(right=0.9, hspace=0.1, wspace=0.05)
    cax = fig.add_axes([0.93, 0.15, 0.02, 0.7])
    cbar = fig.colorbar(im, cax=cax)
    cbar.set_label(cbar_label)

    fig.suptitle(suptitle)
    return fig, axs


def roc(real, pred):
    fpr_real, tpr_real, _ = roc_curve(real, pred) # FIXME: gehört das hier rein?

    fig, axs = plt.subplots()

    axs.plot(fpr_real, tpr_real)
    axs.set_xlabel('False Positive Rate')
    axs.set_ylabel('True Positive Rate')
    axs.set_title('ROC-Curve')
    plt.tight_layout()
    return fig, axs
       

def pr(real, pred):
    precision, recall, _ = precision_recall_curve(real, pred)
    fig, axs = plt.subplots()

    axs.plot(recall, precision)
    axs.set_xlabel('Recall')
    axs.set_ylabel('Precision')
    axs.set_title('Precision-Recall Curve')

    plt.tight_layout()
    return fig, axs


def histogram(data, titles, colnames, x_label): #ylabel ist immer Häufigkeit, dh irrelevant
    n_subplots = len(data)

    fig, axs = plt.subplots(n_subplots, 1, figsize=(10, 3*n_subplots))

    for i in range(n_subplots):
        for j in range(len(colnames)):
            axs[i].hist(data[i][:, j], bins=50, alpha=0.5, label=colnames[j])
            axs[i].legend(loc="upper right", title=titles[i])
            axs[i].set_yticks([])
        if i+1 == n_subplots:
            direction = "inout"
            axs[i].set_xlabel(x_label)
        else:
            direction = "in"
            axs[i].set_xticklabels([])
        axs[i].tick_params(axis="x", direction="in")

    fig.suptitle("Histogram")
    plt.tight_layout()
    fig.subplots_adjust(hspace=0)
    return fig, axs


def skz(data, labels, dim_labels): # vergleich von statistischen kennzahlen
    fig, ax = plt.subplots(figsize=(10, 5))
    data = [(np.mean(data[i], axis=0), np.std(data[i], axis=0)) for i in range(len(data))]

    width = 0.35  # width of the bars
    colors = ["tab:orange", "tab:blue"] # FIXME: where do the colors come from?

    x_ = [[x+(width/2), x-(width/2)] for x in range(len(labels))]

    for i, (means, stds) in enumerate(data):
        for j in range(len(dim_labels)):
            ax.bar(x_[i][j], means[j], width, yerr=stds[j],
                    label=dim_labels[j] if i == 0 else '_nolegend_', 
                    alpha=0.7, color=colors[j])

    ax.set_ylabel('Average')
    ax.set_title('Vergleich der statistischen Kennzahlen')
    ax.set_xticks(list(range(len(labels))))
    ax.set_xticklabels(labels)
    ax.legend(loc="lower right")

    plt.tight_layout()
    return fig, ax


def confusion_matrix(y_true, y_pred, classes):
    fig, axs = plt.subplots(figsize=(7, 4))

    cm = conf_matrix(y_true, y_pred)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]  # Normalisierung

    im = axs.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
    axs.set_title('Confusion Matrix')
    plt.colorbar(im, ax=axs)
    tick_marks = np.arange(len(classes))
    axs.set_xticks(tick_marks)
    axs.set_yticks(tick_marks)
    axs.set_xticklabels(classes)
    axs.set_yticklabels(classes)

    fmt = '.2f'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            axs.text(j, i, format(cm[i, j], fmt),
                     ha="center", va="center",
                     color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    return fig, axs


