#!/usr/bin/env python

import os
import subprocess
import pickle
import itertools
import math

from PIL import Image, ImageDraw
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib.lines import Line2D
import pandas as pd
import librosa
from sklearn.metrics import roc_curve, auc, precision_recall_curve
from sklearn.metrics import confusion_matrix as conf_matrix

from feature_extractor import FeatureExtractor
from utils import get_last_model_names
from plots import *

#-----------------------------------------------------------------------
# Animate Lip Movements
#-----------------------------------------------------------------------

def animate(marker_data, audio_file, img_size=200, connect=False, res_name=None, anim_dir="animation", **feature_params):
    ''' Generates a visual representation of marker data synchronized with an audio file.
    Saves the result as a GIF and a .mp4 video file.
    `marker_data` can be a string (path to marker_file) or an array (generator output) '''
    os.makedirs(anim_dir, exist_ok=True)

    marker_set = feature_params.get("marker_set")

    signal, sr = librosa.load(audio_file)
    duration = np.around(len(signal)/sr, decimals=3)  # Ignore Nanoseconds
    print("audiofile:", audio_file)
    print("duration: ", duration)

    if res_name is None:
        res_name = audio_file.split("/")[-1].split(".")[0]

    if type(marker_data) == str: # Case filepath
        fex = FeatureExtractor(**feature_params)
        marker_df = fex.marker_features(file=marker_data)#, extracted=extracted) # TODO: extracted

        duration = marker_df.index.values[-1] - marker_df.index.values[0]
        avg_frame_time = duration / len(marker_df)

    else:  # Case generator output 
        t_step = 0.0083125  # Fixed frame time
        mf_index = [i*t_step + (t_step/2) for i in range(len(marker_data))]
        
        columns = []
        for m in marker_set:
            columns.extend([m.lower()+"_x", m.lower()+"_y", m.lower()+"_z"])
        marker_df = pd.DataFrame(marker_data, columns=columns, index=mf_index)

    # left-right (x) and up-down (z) coordinates needed; forward-backward (y) not
    cols = [c for c in marker_df.columns if "y" not in c]
    marker_df = marker_df[cols]

    signal, sr = librosa.load(audio_file)
    duration = np.around(len(signal)/sr, decimals=3)  # Ignore nanoseconds

    marker_df = marker_df.interpolate(method="backfill")
    marker_df = marker_df.interpolate(method="ffill")

    # Scale to image coordinates
    marker_df = 2*(marker_df+(img_size//2)).astype("int")

    for i in range(len(marker_set)):
        col_x = cols[2*i]
        col_y = cols[(2*i)+1]
        col_name = col_x.split("_")[0]
        marker_df[col_name] = list(zip(marker_df[col_x], marker_df[col_y]))
    
    marker_df = marker_df[[m.lower() for m in marker_set]]

    marker_df.loc[0.0] = marker_df.iloc[0]
    marker_df = marker_df.sort_index()
    
    gif = []
    for index, row_ in marker_df.iterrows():
        img = Image.new(mode='L', size=(img_size*2, img_size*2), color=255)
        draw = ImageDraw.Draw(img)
        
        row = row_.values
        points_xy = list(row)
        
        r = round((img_size/100) / 2) # Point radius depending on img-size
        if r < 1:
            draw.point(xy=points_xy, fill="black")
        else:
            for x, y in points_xy:
                draw.ellipse(xy=[x-r, y-r, x+r, y+r], fill="black")

        if connect:
            groups = {}
            for i, p in row_.items():
                gr_name = i[:-1]  # mou1, ch2 ...
                groups.setdefault(gr_name, []).append(p)
                
            if "mou" in groups.keys():
                groups["mou"].append(groups["mou"][0]) # Close the circle

            for k, v in groups.items():
                draw.line(xy=v, width=2)

        img = img.transpose(method=1) # Flip top bottom
        gif.append(img.convert("P"))

    gif_path = os.path.join(anim_dir, f"{res_name}.gif")
    gif[0].save(gif_path, save_all=True, optimize=False, append_images=gif[1:], loop=1, duration=8.3125)  # duration in milliseconds

    vid_path = os.path.join(anim_dir, f"{res_name}.mp4")
    commandstr = f"ffmpeg -r 120.3 -stream_loop -1 -i {gif_path} -i {audio_file} -map 0 -map 1:a " + \
                 f"-c:v libx265 -crf 26 -preset fast -pix_fmt yuv420p " + \
                 f"-c:a aac -movflags +faststart -shortest {vid_path}"
    subprocess.run(commandstr, shell=True)


#-----------------------------------------------------------------------
# Plot the train-progress
#-----------------------------------------------------------------------

def get_step_size(n_ticks, max_bins=15):
    ''' Get the step size for x-ticks for matplotlib '''
    if n_ticks == 0: return 0, 0
    total_ticks = n_ticks + math.ceil(n_ticks / 10)  # Überschuss

    exponent = math.floor(math.log10(abs(total_ticks)))
    lower_bound = 10 ** exponent
    mid_bound = 5 * 10 ** exponent
    upper_bound = 10 ** (exponent + 1)
    print("lower bound", lower_bound)
    print("mid bound", mid_bound)
    print("upper bound", upper_bound)

    if abs(total_ticks - lower_bound) <= abs(total_ticks - mid_bound):
        step_size = lower_bound/10
    elif abs(total_ticks - mid_bound) <= abs(total_ticks - upper_bound):
        step_size = mid_bound/10
    else:
        step_size = upper_bound/10

    step_size = max(1, round(step_size))

    if math.floor(total_ticks/step_size) > max_bins:  # too many ticks
        step_size += step_size

    print("step size:", step_size)

    return step_size, total_ticks


def plot_history(history, average_over_steps=False, smoothing=False, show=False, target=None):
    ''' Plot a train-run history to vizualize the train progress. 
    `history` can be a filename or a pandas.DataFrame of the history.'''
    if type(history) == str:
        if os.path.exists(history):
            history = pd.read_csv(history)
        else:
            raise Exception(f"History file not found: {history}")

    # Group columns for separate plots
    train_cols = [c for c in history.columns if 'test' not in c and 'epoch' not in c]
    test_cols = [c for c in history.columns if 'test' in c and 'epoch' not in c]

    # Split columns into loss and predictions
    train_loss_cols = [c for c in train_cols if 'loss' in c]
    train_pred_cols = [c for c in train_cols if 'pred' in c]
    test_loss_cols = [c for c in test_cols if 'loss' in c]
    test_pred_cols = [c for c in test_cols if 'pred' in c]

    # Corresponding epoch columns
    train_loss_epoch_cols = [c+'_epoch' for c in train_loss_cols]
    train_pred_epoch_cols = [c+'_epoch' for c in train_pred_cols]
    test_loss_epoch_cols = [c+'_epoch' for c in test_loss_cols]
    test_pred_epoch_cols = [c+'_epoch' for c in test_pred_cols]

    # Subplots
    configs = [(train_loss_cols, train_pred_cols, train_loss_epoch_cols, train_pred_epoch_cols, 'Training'),
               (test_loss_cols, test_pred_cols, test_loss_epoch_cols, test_pred_epoch_cols, 'Test') if test_cols else None]
    n_subplots = [sum([1 if config else 0 for config in configs]), 2 if train_pred_cols else 1]
    fig, axs = plt.subplots(*n_subplots, figsize=(15,8))
    if n_subplots[0] == 1:
        axs = [axs]
    elif n_subplots[1] == 1:  # case G pretrain history: only G loss in 1 plot.
        axs = [[ax] for ax in axs]

    # Plotting
    for i, config in enumerate(configs):
        loss_cols, pred_cols, loss_epoch_cols, pred_epoch_cols, title = config

        # Average over epoch if required
        history_ = history[loss_cols+pred_cols+loss_epoch_cols+pred_epoch_cols].copy() # subselection of history
        if average_over_steps or title == "Test":  # Validation runs always averaged over epochs
            for cols, epoch_cols in [(loss_cols, loss_epoch_cols), (pred_cols, pred_epoch_cols)]:
                for col, ecol in zip(cols, epoch_cols):
                    avg_history = history_.groupby(history_[ecol]).agg("mean").reset_index()[[col, ecol]]
                    history_[[col, ecol]] = avg_history

        history_ = history_.dropna().reset_index(drop=True) # FIXME: überprüfen ob das fehler gibt.
        history_ = history_.astype({k:"int" for k in history_.columns if "epoch" in k})

        # Exclude outliers if required
        if title != "Test":
            upper_limit = history_.quantile(99/100.0)
            history_ = history_[history_ <= upper_limit]
            history_ = history_.dropna().reset_index(drop=True)

        # Smoothing
        if smoothing:
            alpha = 0.5 if average_over_steps else 0.05
            history_[loss_cols] = history_[loss_cols].ewm(alpha=alpha).mean()
            history_[pred_cols] = history_[pred_cols].ewm(alpha=alpha).mean()
            history_ = history_.dropna().reset_index(drop=True)

        # Plotting
        if average_over_steps or title == "Test":
            for j, ax in enumerate(axs[i]):
                for col, ecol in zip(loss_cols, loss_epoch_cols):
                    ax.plot(history_[ecol], history_[col].dropna(), label=col, marker="o", markersize=2, linewidth=1, alpha=0.7, linestyle="--")

            # X-Axis
            n_epochs = len(history_[ecol])*max(1, history_[ecol][0]+1)  # n epoch entries * val_interval
            step, _ = get_step_size(n_epochs)
            # x-ticks like epoch cols
            x_ticks = np.insert(np.array(history_[ecol]).astype(int), 0, 0)
            x_labels = np.insert(np.array(history_[ecol]).astype(int), 0, 0)
            val_interval = x_ticks[1]-x_ticks[0]+1
            step = int(step/val_interval)
            x_ticks = x_ticks[::step]
            x_labels = x_labels[::step]
            
        else:
            # Plot loss
            for j, ax in enumerate(axs[i]):
                for col, ecol in zip(loss_cols, loss_epoch_cols):
                    axs[i][0].plot(history_[col].dropna(), label=col, alpha=0.7)
            # Plot predictions
            if len(axs[i]) > 1:
                for col, ecol in zip(pred_cols, pred_epoch_cols):
                    axs[i][1].plot(history_[col], label=col, alpha=0.7)

            # x-ticks at start of new epochs
            epoch_starts = history_[loss_epoch_cols[0]].dropna().diff().ne(0)
            n_epochs = history_[loss_epoch_cols[0]].dropna().max()
            steps_per_epoch = int(round(len(history_)/n_epochs))

            step, total_ticks = get_step_size(n_epochs)
            x_ticks = list(np.where(epoch_starts)[0])
            x_labels = list(history_[loss_epoch_cols[0]].iloc[x_ticks])
            while len(x_ticks) < total_ticks:
                x_ticks.append(x_ticks[-1]+steps_per_epoch)
                x_labels.append(x_labels[-1]+1)
            x_ticks = x_ticks[::step]
            x_labels = np.array(x_labels[::step]).astype(int)


        for j, ax in enumerate(axs[i]):
            axlabel = "Predictions" if j==1 else "Loss"
            ax.set_xticks(x_ticks)
            ax.set_xticklabels(x_labels)
            ax.set_title(f"{title} {axlabel}")
            ax.set_xlabel("Epoch")
            ax.set_ylabel(axlabel)
            ax.legend()

    plt.tight_layout(pad=1.5, h_pad=1.7, w_pad=1.7, rect=(0.01, 0.01, 0.99, 0.99))
    plt.subplots_adjust(wspace=0.15)
    if show:
        plt.show()

    if target is not None:
        plt.savefig(target, dpi=1000)

    return fig


# TESTING
#histories = ["logs/3_default_run/history_G.csv", "logs/3_default_run/history_D.csv", "logs/3_default_run/history.csv", "logs/3_default_run/history_FT.csv"]
#for h in histories:
#    plot_history(h, show=True)
#exit()

#-----------------------------------------------------------------------
# Plot the validation-run data
#-----------------------------------------------------------------------

n_subplots = {
    "residuals": 4,
    "boxplot": 2,
    "heatmap": 4,
    "roc": 1,
    "histogram": 2,
    "pr": 1,
    "confusion": 1,
    "skz": 1,
}


def create_layout(types):
    max_size = (4, 4)

    subplot_counts = [(n_subplots[t], t) for t in types]
    subplot_counts.sort(key=lambda x: x[0], reverse=True)

    
    d = {}
    occupied = np.array([0]*max_size[1]) # if all are filled looks like [4, 4, 4, 4] for each row the number of fields are taken.
    for n, k in subplot_counts:
        if n == 4: # those of 4 are just placed, because they come first.
            col = occupied[0] if occupied[0] != 4 else occupied[2]
            row = 0 if occupied[0] != 4 else 2
            d[k] = (row, col)
            occupied[row] += 2
            occupied[row+1] += 2
        else: # theoretically: size 2 can be horizontally or vertically (depending on remaining number of plots)
            next_free_row = np.argwhere(np.array(occupied) != 4).min()
            col = occupied[next_free_row]
            row = next_free_row
            d[k] = (row, col)
            occupied[row] += n
            
    max_row = len([1 for i in occupied if i != 0])  # zeilen
    max_col = max(occupied)  # spalten

    return d, (max_row, max_col)


def plot_d_val(history, *types, show=False, path=None):
    with open('h.pickle', 'rb') as f:
       history = pickle.load(f)

    if path is not None:
        path = os.path.join(path, "plots/")
        os.makedirs(path, exist_ok=True)

    # Get and prepare data
    raw_data = history.get_last(n=0, by_group=True, groups=['raw'])['raw']
    pred_real = raw_data['pred_real_raw']
    pred_fake = raw_data['pred_fake_raw']
    pred_mism = raw_data['pred_mismatched_raw']

    pred_real = np.array(list(itertools.chain(*pred_real)))
    pred_fake = np.array(list(itertools.chain(*pred_fake)))
    pred_mism = np.array(list(itertools.chain(*pred_mism)))

    pred_real = pred_real.reshape(-1, 2)  # shape is now (len(dataset) * seq_len, 2)
    pred_fake = pred_fake.reshape(-1, 2)
    pred_mism = pred_mism.reshape(-1, 2)

    # Plot
    if "boxplot" in types:
        data = [pred_real[...,1], pred_fake[...,0], pred_mism[...,0]]
        titles = ["Real Samples", "Fake Samples", "Mismatched Fake Samples"]
        fig, axs = boxplot(data, titles=titles, y_label="Prediction")

        if show:
            plt.show()
        if path:
            plt.savefig(os.path.join(path, "val_plot_boxplot.jpg"), dpi=1000)

    if "heatmap" in types:
        pred_real_seq = pred_real.reshape(-1, 71, 2)
        pred_fake_seq = pred_fake.reshape(-1, 71, 2)
        pred_mism_seq = pred_mism.reshape(-1, 71, 2)

        true_real = np.zeros_like(pred_real_seq)
        true_real[..., 1] = 1
        true_fake = np.zeros_like(pred_real_seq)
        true_fake[..., 0] = 1
        
        residuals_real = np.abs(true_real - pred_real_seq)
        residuals_fake = np.abs(true_fake - pred_fake_seq)
        residuals_mism = np.abs(true_fake - pred_mism_seq)

        data = [residuals_real, residuals_fake, residuals_mism]
        titles = ["Real Samples", "Fake Samples", "Mismatched Samples"]
        colnames = ["Predicted Real", "Predicted Fake"]

        fig, axs = heatmap(data, titles=titles, colnames=colnames, 
                           cbar_label="Absolute Residuals", 
                           suptitle="Residuals Heatmaps")
        if show:
            plt.show()
        if path:
            plt.savefig(os.path.join(path, "val_plot_heatmap.jpg"), dpi=1000)

    if "roc" in types: 
        true_real = np.ones_like(pred_real[:,1])
        true_fake = np.zeros_like(pred_real[:,1])
        y_true = np.concatenate((true_real, true_fake, true_fake))
        y_pred = np.concatenate((pred_real[:,1], pred_fake[:,0], pred_mism[:,0]))
        #fpr_real, tpr_real, _ = roc_curve(y_true, y_pred)

        fig, axs = roc(y_true, y_pred)

        if show:
            plt.show()
        if path:
            plt.savefig(os.path.join(path, "val_plot_ROC.jpg"), dpi=1000)

    if "pr" in types:
        true_real = np.ones_like(pred_real[:, 1])
        true_fake = np.zeros_like(pred_real[:, 1])
        y_true = np.concatenate((true_real, true_fake, true_fake))
        y_pred = np.concatenate((pred_real[:,1], pred_fake[:,0], pred_mism[:,0]))
        #precision, recall, _ = precision_recall_curve(y_true, y_pred)
        fig, axs = pr(y_true, y_pred)

        if show:
            plt.show()
        if path:
            plt.savefig(os.path.join(path, "val_plot_PR.jpg"), dpi=1000)

    if "histogram" in types:
        data = [pred_real, pred_fake, pred_mism]
        titles = ["Real Samples", "Fake Samples", "Mismatched Samples"]
        colnames = ["Predicted Real", "Predicted Fake"]
        fig, axs = histogram(data, titles=titles, colnames=colnames, x_label="Confidence")

        if show:
            plt.show()
        if path:
            plt.savefig(os.path.join(path, "val_plot_histogram.jpg"), dpi=1000)

    if "skz" in types: # vergleich von statistischen kennzahlen (wasauchimmerdasist)
        data = [pred_real, pred_fake, pred_mism]
        labels = ['Real', 'Fake', 'Mismatched'] # labels for x-axis
        colnames = ["Predicted Fake", "Predicted Real"]

        fig, axs = skz(data, labels=labels, dim_labels=colnames)

        if show:
            plt.show()
        if path:
            plt.savefig(os.path.join(path, "val_plot_skz.jpg"), dpi=1000)

    if "confusion" in types:
        true_real = np.ones_like(pred_real[:,0])
        true_fake = np.zeros_like(pred_real[:,0])
        y_true = np.concatenate((true_real, true_fake, true_fake)) 
        y_pred = np.concatenate((np.argmax(pred_real, axis=-1), 
                                np.argmax(pred_fake, axis=-1),
                                np.argmax(pred_mism, axis=-1)))
        classes = ['Real', 'Fake']

        #cm = confusion_matrix(y_true, y_pred)
        #cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]  # Normalisierung

        fig, axs = confusion_matrix(y_true, y_pred, classes=classes)

        if show:
            plt.show()
        if path:
            plt.savefig(os.path.join(path, "val_plot_confusion.jpg"), dpi=1000)

  
def plot_g_val(history, *types, show=False, path=None, marker_names=None):
    ''' Validation plot for generator output during validation run. '''
    # TESTING THE CODE
    #with open('h.pickle', 'wb') as f:
    #    pickle.dump(history, f)
    #exit()
    with open('h.pickle', 'rb') as f:
        history = pickle.load(f)

    raw_data = history.get_last(n=0, by_group=True, groups=['raw'])['raw']
    real_data = raw_data['real_marker_data']
    gen_data = raw_data['generated_marker_data']
    
    real_data = np.array(list(itertools.chain(*real_data)))
    gen_data = np.array(list(itertools.chain(*gen_data)))

    # FIXME: before run with fixed dataset bug
    real_data = real_data[:1000]
    gen_data = gen_data[:1000]

    # Subset of samples:
    chosen_indices = np.random.choice(real_data.shape[0], size=real_data.shape[0]//10, replace=False)
    real_data = real_data[chosen_indices]
    gen_data = gen_data[chosen_indices]
    
    # Subset of markers
    #marker_indices = slice(0,9)
    #real_data_subset = real_data[..., marker_indices]
    #gen_data_subset = gen_data[..., marker_indices]

    # reduce sequence length (every 2nd timestep)
    #real_data = real_data[:, ::5, :]
    #gen_data = gen_data[:, ::5, :]

    if "plotti" in types: # heatmap of differences  FIXME: interessant, man braucht aber richtige daten.
        diff = np.abs((real_data-gen_data))

        diff = np.mean(diff, axis=0)
        #diff = diff[0, ::3, :] # zum ausprobieren/testen

        diff_x = diff[:, ::3].T
        diff_y = diff[:, 1::3].T
        diff_z = diff[:, 2::3].T

        fig, ax = plt.subplots(1, 3)
        ax[0].imshow(diff_x, cmap="hot", aspect="auto")
        ax[0].set_title("X-Coordinates")
        ax[1].imshow(diff_y, cmap="hot", aspect="auto")
        ax[1].set_title("Y-Coordinates")
        im = ax[2].imshow(diff_z, cmap="hot", aspect="auto")
        ax[2].set_title("Z-Coordinates")

        cax = fig.add_axes([0.95, 0.1, 0.02, 0.7])
        cbar = fig.colorbar(im, cax)
        cbar.set_label("abs diff")

        plt.show()
        exit()

    if "plotti2" in types:
        diff = np.abs((real_data-gen_data))

        diff = np.mean(diff, axis=0)
        #diff = diff[0, ::3, :] # zum ausprobieren/testen

        diff_x = diff[:, ::3].T
        diff_y = diff[:, 1::3].T
        diff_z = diff[:, 2::3].T

        fig, ax = plt.subplots(1, 3)

        # x achse timestep
        # y achse unterschied
        # 1 kurve pro marker
        # gemittelt über samples?

 
        diff_corr = np.corrcoef((real_data-gen_data).mean(axis=-1).T)

        fig, axs = heatmap(data=[real_corr, gen_corr, diff_corr], titles=[""],
                           colnames=["Real", "Generated", "Difference"])
        plt.show()

    
    if "scatter" in types: #FIXME: bisserl schöner machen, ansonsten nützlich
        print("scatter")
        fig, axs = plt.subplots(1,3, figsize=(10,7))
        
        for j in range(real_data.shape[1]):
            if j % 2 == 0: continue
            for i in range(0, real_data.shape[-1], 3):  # nur die ersten 3 Marker
                # front view:
                axs[0].scatter(real_data[0, j, i], real_data[j, 0, i+2], s=1.5, c='green', alpha=0.7)
                axs[0].scatter(gen_data[0, j, i], gen_data[j, 0, i+2], s=1.5, c='red', alpha=0.7)
                axs[0].set_title("Front View")
                # side view:
                axs[1].scatter(real_data[0, j, i+1], real_data[j, 0, i+2], s=1.5, c='green', alpha=0.7)
                axs[1].scatter(gen_data[0, j, i+1], gen_data[j, 0, i+2], s=1.5, c='red', alpha=0.7)
                axs[1].set_title("Side View")
                # top view:
                axs[2].scatter(real_data[0, j, i], real_data[j, 0, i+1], s=1.5, c='green', alpha=0.7)
                axs[2].scatter(gen_data[0, j, i], gen_data[j, 0, i+1], s=1.5, c='red', alpha=0.7)
                axs[2].set_title("Top View")
        plt.show()
        exit()

    if "3dscatter" in types:
        print("3dscatter")
        fig = plt.figure(figsize=(12, 6))
        ax = fig.add_subplot(111, projection='3d')
        for j in range(len(real_data)):
            if j > 10: break
            for i in range(0, real_data.shape[-1], 3):  # nur die ersten 3 Marker
                ax.scatter(real_data[j, 0, i], 
                           real_data[j, 0, i+1], 
                           real_data[j, 0, i+2], s=1.5, c='green')
                ax.scatter(gen_data[j, 0, i], 
                           gen_data[j, 0, i+1],
                           gen_data[j, 0, i+2], s=1.5, c='red')
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        plt.show()
        exit()

    if "heatmap" in types: # heatmap der seqzenzkorrelation
        raise NotImplementedError("Heatmap for generator validation not implemented yet.")

        real_data_avg = real_data.mean(axis=-1)
        gen_data_avg = gen_data.mean(axis=-1)
        real_corr = np.corrcoef(real_data_avg.T)
        gen_corr = np.corrcoef(gen_data_avg.T)

        diff_corr = np.corrcoef((real_data-gen_data).mean(axis=-1).T)

        fig, axs = heatmap(data=[real_corr, gen_corr, diff_corr], titles=[""],
                           colnames=["Real", "Generated", "Difference"])
        plt.show()

        fig, ax = plt.subplots(1, 3, figsize=(15, 6))
        ax[0].imshow(real_corr, cmap='hot', interpolation='nearest')
        ax[0].set_title('Korrelationsheatmap der echten Daten')
        ax[1].imshow(gen_corr, cmap='hot', interpolation='nearest')
        ax[1].set_title('Korrelationsheatmap der generierten Daten')
        ax[2].imshow(diff_corr, cmap='hot', interpolation='nearest')
        ax[2].set_title('Korrelationsheatmap des unterschieds')
        plt.show()
        exit()


    if "timeseries" in types:  # interessant, es fehlt: chart pro marker?
        print("timeseries")
        plt.figure(figsize=(12, 6))
        real = real_data[0, :, :]
        gen = gen_data[0, :, :]
        for i in range(real_data.shape[-1]):
            if i > 2:
                break
            plt.plot(real[:, i]-gen[:, i], label=f'Real data {i+1}')

        #for i in range(2):
        #    plt.plot(real_data_avg[i], label=f'Real data {i+1}')
        #    plt.plot(real_data_avg[i], label=f'Generated data {i+1}')
        plt.title("Zeitreihe for real und generated data")
        plt.xlabel("Timestep")
        plt.ylabel("Markerposition")
        plt.legend()
        plt.show()
        exit()


    if "histogram" in types: # FIXME: interessant, noch viel zu tun.
        print("histogram")
        plt.figure(figsize=(12, 6))
        plt.figure(figsize=(12, 6))
        plt.hist(real_data.mean(axis=1).ravel(), bins=50, alpha=0.5,
                    label='Real data', color='blue')
        plt.hist(gen_data.mean(axis=1).ravel(), bins=50, alpha=0.5, 
                    label='Generated data', color='red')
        plt.title('Verteilung der Markerpositionen')
        plt.xlabel('Markerposition')
        plt.legend()
        plt.show()

        exit()


# Usage examples

all_plots = ["histogram", "heatmap", "boxplot", "residuals", "roc", "pr", "skz", "confusion"]
selection1 = ["histogram", "confusion", "residuals"]
selection2 = ["roc", "pr", "skz"]
selection3 = ["heatmap"]

g_all_plots = ["timeseries", "3dscatter", "histogramm", "heatmap", "violin", "boxplot"]

#plot_g_val("luuu", "plotti", show=True)
#plot_g_val("lulu", *g_all_plots, show=True)
#plot_d_val("hihi", *all_plots, show=True)
#exit()



# Plot:
#history_file = "logs/1_default/history.csv"
#plot_history(history_file, smoothing_factor=1.0, percentile=98, average_over_steps=True, show=True)
#exit()

# Animate Lip movements:
# def animate(marker_data, img_size=200, connect=False, audio_file=None, res_name=None, anim_dir="animation", **feature_params):

##Ses01M_script01_1_F018.txt
##Ses01M_impro07_M003.txt
#turn = "Ses02F_script03_1"
#take = "F021"
#sesnum = 2
##turn = "Ses01M_script01_1"
##take = "M018"
#dataroot = f"/Users/lea/uni/praktikum/dataset/IEMOCAP_full_release/Session{sesnum}/sentences/MOCAP_rotated"
#audioroot = f"/Users/lea/uni/praktikum/dataset/IEMOCAP_full_release/Session{sesnum}/sentences/wav"
#marker_set = ["MOU1","MOU2","MOU3","MOU4","MOU5","MOU6","MOU7","MOU8","RC1","RC4","LC1","LC4","CH1","CH2","CH3"]
#
#marker_path = os.path.join(dataroot, turn, turn+"_"+take+".txt")
#audio_file = os.path.join(audioroot, turn, turn+"_"+take+".wav")
#
#animate(marker_data=marker_path, marker_set=marker_set, connect=True, audio_file=audio_file, img_size=300)
##animate(marker_data=marker_path, marker_set=marker_set, connect=True, img_size=300)

