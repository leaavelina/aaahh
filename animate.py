#!/usr/bin/env python

# TODO: predict from checkpoint

import glob
import os
import ruamel.yaml as yaml

import argparse
from argparse import Namespace
import pathlib
import sys
from collections import OrderedDict

import numpy as np
import torch

import matplotlib.pyplot as plt

from csg import Generator, Discriminator
from feature_extractor import FeatureExtractor, emo_enum
from viz import animate
from utils import get_last_model_names, get_load, str2bool
from trainer import Trainer

def main(args):
    device = torch.device("cpu")

    # TODO: Prints / Logger

    audio_file = args.audio_file
    marker_file = args.marker_file
    feature_params = args.feature_params

    if marker_file is None:  # marker data not existent, has to be generated (-> case model output)
        fex = FeatureExtractor(**feature_params)
        af = fex.audio_features(audio_file, frame=False, extracted=False)

        # Dimensions
        seq_len = feature_params["seq_len"]
        noise_dim = feature_params["noise_dim"]
        n_mf = len(feature_params["marker_set"])*3

        n_pad = seq_len - (len(af) % seq_len)  # padding so that shape (n_samples+n_pad, seq_len, n_af) is possible
        padded = np.tile(af[-1],(n_pad, 1))
        af = np.concatenate((af, padded), axis=0)

        e = torch.zeros(*list(af.shape[:-1]), 6)
        inputs = np.concatenate((af, e), axis=-1)  

        # create a batch from the whole audio data
        batch_size = int(len(inputs)/seq_len)
        batched_inputs = torch.tensor(inputs).reshape(batch_size, seq_len, inputs.shape[-1])
        noise = Trainer.generate_noise(batched_inputs.shape, noise_dim)

        inputs = torch.cat((batched_inputs, noise), dim=-1).float().to(device)
        inputs.require_grad = False

        # Init model
        G = Generator(input_size=inputs.shape[-1],
                      output_size=n_mf,
                      hidden_size=args.model_params["g_hidden_size"],
                      num_layers = args.model_params["g_num_layers"])

        # Load generator weights
        G.load_state_dict(torch.load(args.load["g"], map_location=torch.device("cpu")))
        #G.to(device=device) TODO: animate auf GPU

        outputs = torch.empty((batch_size, seq_len, n_mf))

        # pass batch
        G.eval()

        # WITH STATES: ????
        #states = None
        #with torch.no_grad():
        #    for i, inp in enumerate(inputs):
        #        output, states = G(inp, states)
        #        outputs[i, :, :] = output

        outputs, _ = G(inputs)

        outputs = outputs.reshape(np.prod(outputs.shape[:2]), *outputs.shape[2:]).detach()
        marker_data = outputs[0:-n_pad] # not interested in padded stuff

    else:  # case dataset vizsualiation
        marker_data=marker_file

    # Default target names
    if args.target_name is None:
        if type(marker_data) is str:
            target_name = marker_data.split("/")[-1].split(".")[0] + "_mrk"
        else: 
            target_name = audio_file.split("/")[-1].split(".")[0] + "_gen"
    else: 
        target_name = args.target_name
    target_dir = "animations/" if args.target_dir is None else args.target_dir


    animate(marker_data=marker_data, img_size=args.img_size,
            connect=args.connect_markers, audio_file=audio_file, 
            anim_dir=target_dir, res_name=target_name, **feature_params)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # Arguments for input file(s)
    parser.add_argument("-a", "--audio-file", required=True,
                        help="path to the audio file of which markers are wanted")
    parser.add_argument("-m", "--marker-file", required=False,
                        help="path to the marker file corresponding to the audio file")
    parser.add_argument("-x", "--extracted", default=None, const=True, type=str2bool, nargs="?",
                        help="Audio-file and marker-file (if given) contain already extracted features")

    # Arguments for target-file
    parser.add_argument("--target-dir", required=False,
                        help="path to directory, where to save the resulting video and gif")
    parser.add_argument("-n", "--target-name", required=False,
                        help="name of the output files (without extension)")

    # Arguments for animation
    parser.add_argument("-s", "--img-size", required=False, default=300, type=int,
                        help="size (in pixel) of the resulting video, always square: (size x size)")
    parser.add_argument("-c", "--connect-markers", required=False, action="store_true", 
                        help="draw lines between marker-points of same group")
  
    # Arguments for Generator
    parser.add_argument("-l", "--load", default=None, nargs="?", const="g_FT", type=str, metavar="GENERATOR",
                        help="load model: -l <modelname> OR -l g[_<tag>] for most recently finetuned (tag=FT), pretrained (tag=PT), trained (no tag)")

    parser.add_argument("--config_file", default="params.yaml", 
                        help="file (yaml or json) for all parameters")


    args_ , remaining = parser.parse_known_args()

    with open(args_.config_file, 'r') as f:
        params_ = yaml.safe_load(f)
    del args_.config_file

    # TODO: anders machen
    args_.__setattr__("feature_params", params_["feature_params"])
    if args_.noise_dim is not None:
        args_.feature_params["noise_dim"] = args_.noise_dim

    args_.__setattr__("model_params", params_["model_params"])
    args_.__setattr__("model_dir", params_["model_dir"])

    if args_.extracted is None:  # FIXME: is this necessary?
        args_.extracted = params_["dataset_patams"]["extracted"]

    # Loading
    if args_.load is not None:  # args.load is a string (not like in train/validate where load argument is a list)
        args_.load = get_load([args_.load], args_.model_dir)
        if args_.load.get("g") is None:
            raise Exception("You need to specify a generator for loading.")
    else:
        if args_.marker_file is None:
            raise Exception("Either generator or marker-file-path needs to be specified.")
        args_.load = False
    
    main(args_)

