#!/usr/bin/envpython

import time
from datetime import datetime
import random
import glob
import os
import subprocess
import logging

import numpy as np
import torch
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from torch.nn.functional import binary_cross_entropy
from torch.distributions.multivariate_normal import MultivariateNormal
import pandas as pd

from utils import timestr, print_configuration
from dataset import SyncLipDataset
from csg import Generator, Discriminator, init_weights, freeze_l0, unfreeze_l0
from loss import ccc_loss


class Trainer:
    def __init__(self, device:str, comment:str, log_dir:str, model_dir:str, noise_dim:int, verbose:int, **params):
        """
        Parameters:
        device: str
            choice of "cpu", "gpu", "mps"
        comment: str
            title of this run / short description
        log_dir: str
            path to directory where to store the history-log files.
        model_dir: str
            path to directory where models are to be saved.
        checkpoint_dir: str Optional
            path to directory where to save checkpoints
        checkpoint_interval: int
            every checkpoint_interval-steps a checkpoint is saved.
        noise_dim: int
            dimension of the noise added to the generator inputs
        verbose: int
            output 0: off, 1: on
        """
        self.device = device
        self.comment = comment
        self.noise_dim = noise_dim 

        # Model directory  # TODO: auslagern?
        self.model_dir = model_dir
        os.makedirs(self.model_dir, exist_ok=True)

        # Log directories (for logs, history, plots)
        self.log_dir = log_dir
        os.makedirs(self.log_dir, exist_ok=True)

        # Logger
        logger = params.get("logger", Logger(params.get("verbose", 0), log_dir=self.log_dir))
        # TODO: test if oben works, in that case that unten is redundant.
        if logger is None:
            self.logger = Logger(self.verbose, log_dir=self.log_dir)
        else: 
            self.logger = logger

        val_interval = params.get("validation_interval", 0)
        self.val_interval = float("inf") if val_interval < 1 else val_interval

        # History
        self.history = History(log_dir=log_dir, comment=comment)
        groups = {"Loss": ["d_loss_real", "d_loss_fake", "d_loss_mismatched", "g_loss"],
                  "Predictions": ["pred_real", "pred_fake", "pred_mismatched"]}
        for grp, vs in groups.items():  # All values have also a "test" pendant.
            groups[grp].extend(["test_" + name for name in vs])
        self.history.set_groups(groups)
    
        # Checkpoints
        cpt_interval = params.get("checkpoint_interval", 0)
        if cpt_interval > 0:
            self.cpt_interval = cpt_interval
            self.cpt_dir = params.get("checkpoint_dir", "./checkpoints/")
            os.makedirs(self.cpt_dir, exist_ok=True)
        else:
            self.cpt_interval = float("inf")

        # Tensorboard
        self.tb_dir = params.get("tb_dir")
        if self.tb_dir is not None:
            self.writer = SummaryWriter(self.tb_dir)
            os.makedirs(self.tb_dir, exist_ok=True)
            print(f"Run Tensorboard with:\n\ttensorboard --logdir=runs\n")

        self.model_init = False
        self.optim_init = False


    def init_model(self, g_input_size, d_input_size, g_hidden_size, d_hidden_size,
                   g_output_size, g_num_layers, d_num_layers, **params):
        """ Create the model and initialize the weights.  """
        self.G = Generator(input_size=(g_input_size), hidden_size=g_hidden_size,
                          output_size=g_output_size, num_layers=g_num_layers)
        self.G.to(device=self.device)
        self.G.apply(init_weights)

        self.D = Discriminator(input_size=(d_input_size), hidden_size=d_hidden_size,
                               output_size=2, num_layers=d_num_layers)
        self.D.to(device=self.device)
        self.D.apply(init_weights)

        self.model_init = True

    def load_model(self, g_path=None, d_path=None, **model_params):
        ''' Loads the generator and/or the discriminator models from given paths.
        Both generator and/or discriminator weights are loaded from the specified paths, or are located using the model name.
        If no path is given for one model, it is initalized with the provided parameters. '''
        if not self.model_init:
            self.init_model(**model_params)

        if g_path is not None:
            self.logger.load_G(g_path)
            self.G.load_state_dict(torch.load(g_path, map_location=self.device))

        if d_path is not None:
            self.logger.load_D(g_path)
            self.D.load_state_dict(torch.load(d_path, map_location=self.device))

    def save_model(self, pretrain=None, finetune=False):
        ''' For saving the models state dicts to the model directory. 
        The 'pretrain' parameter determines which model(s) to save - Generator, Discriminator, or both.
        If `pretrain` is None, both models are saved; if it's 'G' or 'D', only that respective model is saved. '''
        if pretrain is not None:
            model = getattr(self, pretrain)
            path = os.path.join(self.model_dir, f"{pretrain}_PT.pth")
            self.logger.save_models(path)
            torch.save(model.state_dict(), path)
        else:
            g_path = os.path.join(self.model_dir, f"G{'_FT' if finetune else ''}.pth")
            d_path = os.path.join(self.model_dir, f"D{'_FT' if finetune else ''}.pth")
            self.logger.save_models(g_path, d_path)
            torch.save(self.G.state_dict(), g_path)
            torch.save(self.D.state_dict(), d_path)

    def save_checkpoint(self, epoch, loss):
        torch.save({
            'epoch': epoch,
            'G_state_dict': self.G.state_dict(),
            'D_state_dict': self.D.state_dict(),
            'g_optim_state_dict': self.g_optim.state_dict(),
            'd_optim_state_dict': self.d_optim.state_dict(),
            'history': self.history,
            #'log_dir': self.log_dir,
            #'comment': self.comment
        }, os.path.join(self.cpt_dir, f"{epoch}.pt"))

    def load_checkpoint(self, epoch):
        if not self.init_model:
            raise RuntimeError("The model needs to be initialized before loading a checkpoint.")
        checkpoint = torch.load(os.path.join(self.cpt_dir, f"{epoch}.pt"))

        self.G.load_state_dict(checkpoint['G_state_dict'])
        self.D.load_state_dict(checkpoint['D_state_dict'])
        self.g_optim.load_state_dict(checkpoint['g_optim_state_dict'])
        self.d_optim.load_state_dict(checkpoint['d_optim_state_dict'])
        self.history = checkpoint['history']
        #self.comment = checkpoint['comment']
        #self.log_dir = checkpoint['log_dir']

    def init_optimizers(self, optim, **optim_params):
        """ Create the optimizers. """
        if not self.model_init:
            raise RuntimeError("The model needs to be initialized before initializing optimizers.")
        self.g_optim = optim(self.G.parameters(), **optim_params)
        self.d_optim = optim(self.D.parameters(), **optim_params)
        self.optim_init = True

    def train_step(self, x, e, x_, fake_x_, fake_e, pretrain=False, step=0):
        ''' Executes a single training step with forward pass, loss computation, and backpropagation.
        Depending on the `pretrain` parameter, only one model may be trained, or both alternatively.
        Results are stored in the History (loss and predictions). '''
        x = x.to(self.device)
        e = e.to(self.device)
        x_ = x_.to(self.device)
        fake_x_ = fake_x_.to(self.device)
        fake_e = fake_e.to(self.device)

        self.G.train() 
        self.D.train()
        self.g_optim.zero_grad()
        self.d_optim.zero_grad()

        # Generate markers
        z = self.generate_noise(x.shape)
        g_input = torch.cat((x, e, z), dim=-1).float()
        g_x_, _ = self.G(g_input)  # fake (generated) x'

        if pretrain == "G":  # case pretrain G: only calculate ccc loss and done
            g_loss = ccc_loss(g_x_, x_.float())
            g_loss.backward()

        else:
            # Real markers
            d_input = torch.cat((x, e, x_), dim=-1).float()
            d_real, _ = self.D(d_input)

            # Fake (generated) markers
            d_fake_input = torch.cat((x, e, g_x_.detach()), dim=-1).float()
            d_fake, _ = self.D(d_fake_input)

            # Fake (mismatched) markers
            d_mism_input = torch.cat((x, fake_e, fake_x_), dim=-1).float()
            d_mism, _ = self.D(d_mism_input)

            # Losses for the discriminator
            d_loss_real = binary_cross_entropy(d_real, self.generate_target(d_real.shape, 1))
            d_loss_fake = binary_cross_entropy(d_fake, self.generate_target(d_fake.shape, 0))
            d_loss_mism = binary_cross_entropy(d_mism, self.generate_target(d_mism.shape, 0))
                   
            total_loss = d_loss_real + d_loss_fake + d_loss_mism
            if pretrain is None: # include g_loss in total loss, else generator loss is irrelevant
                g_loss = torch.nn.functional.binary_cross_entropy(d_fake, self.generate_target(d_fake.shape, 1))
                total_loss += g_loss

            total_loss.backward()

        # Apply gradients
        if pretrain is not None:
            getattr(self, pretrain.lower()+"_optim").step()
        else:
            if step % 2 == 0:
                self.g_optim.step()
            else: 
                self.d_optim.step()

        # History
        if pretrain == "G" or pretrain is None:
            self.history.log(logs={"g_loss": g_loss.item()}) 
        if pretrain == "D" or pretrain is None:
            self.history.log(
                logs={"d_loss_real":d_loss_real.item(), 
                      "d_loss_fake":d_loss_fake.item(),
                      "d_loss_mismatched":d_loss_mism.item(),
                      "pred_real":d_real.argmax(dim=-1).float().mean().item(), 
                      "pred_fake":d_fake.argmax(dim=-1).float().mean().item(),
                      "pred_mismatched": d_mism.argmax(dim=-1).float().mean().item()})


    def train(self, epochs:int, data:object, test_data:object=None, start_ep:int=0, pretrain=None, finetune:bool=False):
        ''' Executes the full training process over `epochs` iterations.
        Includes training steps, logging, validation (if test data provided), and model saving upon completion. '''
        if not self.model_init:
            raise RuntimeError("The model needs to be initialized.")
        if not self.optim_init:
            raise RuntimeError("The optimizers needs to be initialized.")
        self.history.reset()

        if finetune: # FIXME: Test this behaviour
            freeze_l0(self.D)
            freeze_l0(self.G)
        else:
            unfreeze_l0(self.D)
            unfreeze_l0(self.G)

        t_start = time.time()
        for ep in range(start_ep, start_ep+epochs):
            self.history.set_epoch(ep)
            ep_start = time.time()
            for step, (x, x_, e, fake_x_, fake_e) in enumerate(data):  # FIXME: rename the variables

                # Train step call
                self.train_step(x, e, x_, fake_x_, fake_e, pretrain=pretrain, step=step)

                # Logs
                last_n = self.history.get_last(1, by_group=True, indices=False)
                self.logger.train(ep=ep, total_ep=start_ep+epochs, step=step, 
                    total_steps=len(data), last_n=last_n, ep_start=ep_start)
                if self.tb_dir is not None:
                    prefix = "Pretrain/" if pretrain is not None else ("Finetune/" if finetune else "Train/")

                    for grp_name, tag_scalars_dict in last_n.items(): # tag_scalars_dict is dict of history-keyword to list of its last N values
                        tag_scalars_dict = {tag: np.mean(scalars) 
                            for tag,scalars in tag_scalars_dict.items() if "test" not in tag}
                        if grp_name == "unassigned": continue  # unassigned is for validation (not training)
                        self.writer.add_scalars(prefix+grp_name+"/",  # eg. Pretrain/Predictions/
                                                {tag: np.mean(scalars) for tag,scalars in tag_scalars_dict.items()},
                                                ep*len(data)+step)
                    self.writer.flush()

            # Validate
            if (ep+1) % self.val_interval == 0 and test_data is not None:
                self.test(data=test_data, ep=ep, start_ep=start_ep, pretrain=pretrain, training=True)

        # Finish
        Logger.finish(pretrain=pretrain, finetune=finetune, start_time=t_start)
        self.save_model(pretrain=pretrain, finetune=finetune)
        self.history.save(suffix=(('_FT' if finetune else '') if pretrain is None else ("_"+pretrain)))

        return self.history

    def test_step(self, x, e, x_, fake_x_, fake_e, pretrain=False, training=False, finetune=False):
        ''' Executes a single test step with forward pass and loss computation, result logging.
        Can be used for testing a specific model during pretraining (`pretrain` = 'G' or 'D') or for testing both models. '''
        self.G.eval()
        self.D.eval()
        x = x.to(self.device)
        e = e.to(self.device)
        x_ = x_.to(self.device)
        fake_x_ = fake_x_.to(self.device)
        fake_e = fake_e.to(self.device)

        with torch.no_grad():
            # Generate markers
            z = self.generate_noise(x.shape)
            g_input = torch.cat((x, e, z), dim=-1).float()
            g_x_, _ = self.G(g_input)  # fake (generated) x'

            if pretrain == "G":  # case pretrain G: only calculate ccc loss and done
                g_loss = ccc_loss(g_x_, x_.float())

            else:
                # Real markers
                d_input = torch.cat((x, e, x_), dim=-1).float()
                d_real, _ = self.D(d_input)

                # Fake (generated) markers
                d_fake_input = torch.cat((x, e, g_x_), dim=-1).float()
                d_fake, _ = self.D(d_fake_input)

                # Mismatched markers
                d_mism_input = torch.cat((x, fake_e, fake_x_), dim=-1).float()
                d_mism, _ = self.D(d_mism_input)

                # losses für Discriminator
                d_loss_real = binary_cross_entropy(d_real, self.generate_target(d_real.shape, 1))
                d_loss_fake = binary_cross_entropy(d_fake, self.generate_target(d_fake.shape, 0))
                d_loss_mism = binary_cross_entropy(d_mism, self.generate_target(d_mism.shape, 0))
                       
                if pretrain is None:
                    g_loss = torch.nn.functional.binary_cross_entropy(d_fake, self.generate_target(d_fake.shape, 1))

        # History
        if pretrain == "G" or pretrain is None:
            self.history.log(logs={"test_g_loss": g_loss.item()}) 
        if pretrain == "D" or pretrain is None:
            self.history.log(
                logs={"test_d_loss_real":d_loss_real.item(), 
                      "test_d_loss_fake":d_loss_fake.item(),
                      "test_d_loss_mismatched":d_loss_mism.item(),
                      "test_pred_real":d_real.argmax(dim=-1).float().mean().item(), 
                      "test_pred_fake":d_fake.argmax(dim=-1).float().mean().item(),
                      "test_pred_mismatched": d_mism.argmax(dim=-1).float().mean().item()})
                     
            if not training:  # Validation data
                self.history.log(
                    logs={"pred_real_raw": d_real.tolist(),
                          "pred_fake_raw": d_fake.tolist(),
                          "pred_mismatched_raw": d_mism.tolist(),
                          "real_marker_data": x_.tolist(),
                          "generated_marker_data": g_x_.tolist()})



    def test(self, data, ep=0, start_ep=0, pretrain=None, training=False, finetune=False):  
        ''' Tests the Generator and Discriminator on the specified data set.
        Used for validation during training or standalone testing. '''
        if not self.model_init:
            raise RuntimeError("The model needs to be initialized.")

        if not training:  # validating (validate.py)
            self.history.reset()
            self.history.update_groups("raw", 
                ["pred_real_raw", "pred_fake_raw", "pred_mismatched_raw",
                 "real_marker_data", "generated_marker_data"])

        t_start = time.time()
        for step, (x, x_, e, fake_x_, fake_e) in enumerate(data):
            self.test_step(x, e, x_, fake_x_, fake_e,
                           pretrain=pretrain, training=training)

            # Prints
            self.logger.test(step=step, total_steps=len(data), last_n={}, t_start=t_start)
        last_n = self.history.get_last(len(data), by_group=True, indices=False)
        self.logger.test(step=step, total_steps=len(data), last_n=last_n, t_start=t_start)

        # Tensorboard
        if self.tb_dir is not None and training:
            prefix = "Pretrain/" if pretrain is not None else ("Finetune/" if finetune else "Train/")
            for grp_name, tag_scalars_dict in last_n.items(): # tag_scalars_dict is dict of history-keyword to list of its last N values
                if grp_name == "unassigned": continue
                tag_scalars_dict = {tag[5:]: np.mean(scalars) for tag,scalars in tag_scalars_dict.items() if "test" in tag}
                self.writer.add_scalars(prefix+f"Val_{grp_name}/",   # Eg. Pretrain/Val_Loss
                                        tag_scalars_dict, ep)
            self.writer.flush()

        if not training:
            return self.history



    def generate_noise(self, shape):
        ''' Generates static noise for GANs, repeated across the sequence. '''
        cov = torch.diag(torch.ones(self.noise_dim))
        noise = MultivariateNormal(torch.zeros(self.noise_dim), covariance_matrix=cov).sample()
        noise = noise.repeat(shape[0], shape[1], 1)
        return noise.to(self.device)

    def generate_target(self, shape, value):
        ''' Generates binary labels for discriminator training. '''
        target = torch.zeros(size=shape, dtype=torch.float32)
        target[..., value] = 1
        return target.to(self.device)


#----------------------------------------------------------------------------
#  History Class  -  collect output & performace values per step 
#----------------------------------------------------------------------------
class History:
    def __init__(self, log_dir=None, comment=None):
        self.history = {} # valuename -> list of values
        self.epoch = {}  # valuename -> list of epoch-numbers (like indices)
        self.epoch_ = 0  # current epoch

        self.groups = {}  # groupname -> list of valuenames
                          # {"group1": ["key1", "key3"], "group2": ["key2"]}
                          # not all valuenames/keywords of history need to be assigned to a group.

        self.log_dir = "./history" if log_dir is None else log_dir
        if not os.path.exists(self.log_dir):
            os.makedirs(self.log_dir)
            

    def set_groups(self, groups):
        ''' initialize the groups dict of the History.
        The dict maps a groupname to a list of keywords in the history dict.
        Example: {"group1": ["keyword1", "keyword4"], "group2": ["keyword2"]}
        Not all keywords in the history dict need to be assigned to a group.
        '''
        self.groups = groups

    def update_groups(self, grp, kw):
        ''' A keyword or list of keywords `kw` is assigned to a group `grp`. '''
        if type(kw) == str:
            self.groups.setdefault(grp, []).append(kw)
        elif type(kw) == list:
            self.groups.setdefault(grp, []).extend(kw)

    def log(self, logs:dict=None):
        ''' Add values to the history.
        For each keyword in logs the value is appended to the respective keyword in the history.
        For all keywords in logs the current epoch is appended to the epoch dict
        '''
        logs = {} if logs is None else logs
        for k, v in logs.items():
            self.history.setdefault(k, []).append(v)
            self.epoch.setdefault(k, []).append(self.epoch_)

    def set_epoch(self, epoch):
        ''' Set the current epoch number. '''
        self.epoch_ = epoch

    def get_last(self, n=1, by_group=False, indices=False, groups=None):
        ''' Get the last `n` values of the history for all keywords in the history.
        If n is 0, then all data will be returned
        by_group: bool
            If True, the history will be splitted into the number of groups and
            each subset of the history will only contain the keywords of the resp. group.
            All keywords that are not assigned a group are placed under "unassigned"
        Returns a dict that maps a group name to the subset of the history 
        containing all keys assigned to that group.
        '''
        if not by_group:
            last_n = {k: (self.history[k][-n:] if not indices 
                        else [self.epoch[k][-n:], self.history[k][-n:]])
                            for k in self.history.keys()}
            return last_n

        res = {}
        # If 'groups' is not specified, the ungrouped values are 
        #   added to the dictionary under the key 'unassigned'.
        if groups is None or not groups:
            res["unassigned"] = {k: (self.history[k][-n:] if not indices else [self.epoch[k][-n:], self.history[k][-n:]]) 
                            for k in self.history.keys() 
                            if all([k not in names for _, names in self.groups.items()])}
            groups = self.groups.keys()

        for grp_name, kws in self.groups.items():
            if grp_name not in groups: continue
            last_of_grp = {k: (self.history[k][-n:] if not indices else [self.epoch[k][-n:], self.history[k][-n:]])
                                for k in kws if k in self.history.keys()}
            if not last_of_grp:
                continue
            res[grp_name] = last_of_grp


        return res

    def reset(self):
        self.history = {}
        self.epoch = {}
        self.epoch_ = 0

    def to_dataframe(self):  # TODO: what about groups
        ''' Convert history data into pandas DataFrame (for plotting and saving to csv)
        Each history keyword has a value column and an epoch (index) column. '''
        max_len = max([len(v) for _,v in self.history.items()])
        data = {}
        for k, v in self.history.items():
            data[k] = v + [np.nan]*(max_len-len(v))
        df = pd.DataFrame(data)
        for k, v in self.epoch.items():
            df[k+"_epoch"] = v+[np.nan]*(max_len-len(v))  
            df[k+"_epoch"] = df[k+"_epoch"].astype('Int32') # cols with NaNs are of type float
        return df

    def save(self, suffix=""):
        ''' Save the history data into a .csv file.
        Two columns per history keyword: keyword values and their respective epochs. '''
        path = os.path.join(self.log_dir, f"history{suffix}.csv")
        dataframe = self.to_dataframe()
        dataframe.to_csv(path, index=False)


    def __repr__(self):
        s = f"Train history for '{self.comment}'\n"
        if not self.history.keys():
            s += "empty history"
            return s
        max_len_k = max([len(key) for key in self.history.keys()])
        for k, v in history.items():
            s += f"{k.ljust(max_len_k)}: "
            s += f"[{v[0:1]:.3f}, {v[1:2]:.3f}, ... , {v[-2:-1]:.3f}, {v[-1:len(v)]:.3f}], "
            s += f"length: {len(v)}\n"
        s += "groups:\n"
        max_len_gr = max([len(gr) for gr in self.groups.keys()])
        for k, v in self.groups.items():
            s += f"{k.ljust(max_len_gr)}: {v}\n"
        return s



class Logger():
    def __init__(self, verbose=1, log_dir=None, training=True):
        self.log_dir = log_dir if log_dir is not None else "."
        os.makedirs(self.log_dir, exist_ok=True)
        filename = f'{datetime.now().strftime("%Y%m%d_%H%M%S")}'
        filename += f"_{'train' if training else 'val'}.log"
        logging.basicConfig(filename=os.path.join(self.log_dir, filename), 
                            level=(logging.ERROR if log_dir is None else logging.INFO),
                            format='%(message)s')
        self.verbose = verbose
        self.last_train_print = ""
        self.training = training

    def _log(self, ep, total_ep, step, total_steps, t_start, last_n, mode):
        ''' Log-line to file, info as if verbosity level max '''
        if mode == "train":
            print_str = f"Epoch {ep+1: 4d}/{total_ep} {int(100*(step+1)/total_steps): 4d}%  "
        else: 
            if not last_n.keys():
                return  # Skip if there are no values to log
            print_str = f"Validation: {int(100*(step+1)/total_steps): 4d}%  "
            
        for grp, val_dict in last_n.items():
            if grp == "unassigned": continue
            if grp == "raw": continue # Model outputs
            print_str += f" {grp}: ["
            first = True
            for k, v in val_dict.items():
                if mode == 'train' and 'test' in k: continue
                if mode == 'test' and 'test' not in k: continue
                print_str += ", " if not first else " "
                k = k.replace("test_", "")
                k = ' '.join([i.capitalize() for i in k.split("_") 
                                if ("loss" not in i and "pred" not in i)])
                print_str += f"{k}: {np.mean(v):.4f}"
                first = False
            print_str +=  " ] "
        print_str += f" |  {timestr(int(time.time()-t_start), 'clock')}"
        logging.info(print_str)
    
    def train(self, ep, total_ep, step, total_steps, ep_start, last_n=None): # ep is in some cases ep+start_ep
        ''' Log-line for a step in a train run.
        Depending on verbosity there is a print of the values or the epoch and progress or nothing.  '''
        last_n = {} if last_n is None else last_n
        self._log(ep, total_ep, step, total_steps, ep_start, last_n, "train")
        if self.verbose > 0:
            if step==0: print("\r"+70*" ", end="") # clear the Line
            print_str = f"\rep {ep+1: 4d}"
            print_str += f"/{total_ep} {int(100*(step+1)/total_steps): 4d}%  "
            print_str += f" |  {timestr(int(time.time()-ep_start), 'clock')}"
            print(print_str, end="    ")  # Handling line overflow by overwriting with blanks
            self.last_train_print = print_str  # For validation: To append validation results later


    def test(self, step, total_steps, t_start, last_n=None):
        ''' Log-line for a step in a test run. '''
        last_n = {} if last_n is None else last_n
        self._log(0, 0, step, total_steps, t_start, last_n, mode="test")
        if self.verbose > 0:
            print_str = self.last_train_print + f" -  " if self.training else "\r"
            print_str += f"Validating {int(100*(step+1)/total_steps): 4d}%  "
            print_str += f" |  {timestr(int(time.time()-t_start), 'clock')}"
            print(print_str, end="   ", flush=True)

    def load_G(self, path):
        msg = "Loaded the Generator from " + path
        logging.info(msg)
        if self.verbose > 0:
            print(msg)

    def load_D(self, path):
        msg = "Loaded the Discriminator from " + path
        logging.info(msg)
        if self.verbose > 0:
            print(msg)

    def save_models(self, *paths):
        msg = f"Saved model{'s' if len(paths) > 1 else ''} to: "
        for path in paths:
            msg += path+" "
        logging.info(msg)
        if self.verbose > 0:
            print(msg)

    @staticmethod
    def finish(pretrain, finetune, start_time):
        
        traintype = "pretraining" if pretrain is not None else (
                    "finetuning" if finetune else "adversarial training")
        print_str = f"\nFinished {traintype} "
        print_str += pretrain+' ' if pretrain is not None else ''
        print_str += f"in {timestr(int(time.time()-start_time), 'sentence')}"
        logging.info(print_str)
        print(print_str)


    def log(self, msg, v=0):  # TODO: verbosity
        logging.info(msg)
        if self.verbose >= v:
            print(msg)


