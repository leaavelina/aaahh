#!/usr/bin/env python

import argparse
import pathlib
import ruamel.yaml as yaml

from data import collect_file_paths
from feature_extractor import FeatureExtractor
from utils import print_configuration

def main(args):
    print("Extract features to files")
    print(print_configuration(vars(args)))

    audio, marker, emo = collect_file_paths(root_dir=args.dataset_root, 
                                        exclude=args.exclude_file, 
                                        extracted=False)
    feature_params = args.feature_params

    fex = FeatureExtractor(**feature_params)
    fex.features_to_files(file_paths=(audio, marker, emo),
                          target_dir=args.target_dir,
                          feature_types=args.feature_types,
                          override=args.override)

    print("Done.")
       


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    # TODO: -all | -m -a -e  (<- all and the others are exclusive)
    parser.add_argument("-m", dest="feature_types", action="append_const", const="marker",
                        help="extract markers")
    parser.add_argument("-a", dest="feature_types", action="append_const", const="audio",
                        help="extract audio features")
    parser.add_argument("-e", dest="feature_types", action="append_const", const="emo",
                        help="extract emotion features")
    parser.add_argument("-all", dest="feature_types", action="store_const", const="all",
                        help="extract all")
    parser.add_argument("-o", "--override", default=False, action="store_true", 
                        help="override extracted files if existing.")
    parser.add_argument("--dataset-root", type=str, required=False, help="path to dataset")
    parser.add_argument("--target-dir", type=str, required=False, help="target for extracted features")
    parser.add_argument("--feature-params", default='{}', help="yaml/json of wanted feature parameters (marker_set, t_step, ...)")
    parser.add_argument("--exclude-file", type=str, required=False, help="file that contains all turn names, that are to be excluded.")
    parser.add_argument("--params-file", default='params.yaml', type=pathlib.Path, help="file (yaml or json) for feature parameters (marker_set, t_step, ...)")
    args_ , remaining = parser.parse_known_args()

    with open(args_.params_file, "r") as f:  # default parameters
        params_ = yaml.safe_load(f)

    # update feature params passed in script call
    feature_params_ = params_["feature_params"]
    feature_params_.update(yaml.safe_load(args_.feature_params))  
    args_.feature_params = feature_params_

    if not args_.feature_types:
        args_.feature_types = "all"
    if not args_.dataset_root:
        args_.dataset_root = params_["dataset_root"]
    if not args_.target_dir:
        args_.target_dir = params_["extracted_dataset_root"]
    if not args_.exclude_file:
        args_.exclude_file = params_["dataset_params"]["exclude"]

    main(args_)
        

