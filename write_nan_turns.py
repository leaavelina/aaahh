#!/usr/bin/env python

import argparse 
import pathlib
import os

import ruamel.yaml as yaml

from feature_extractor import FeatureExtractor
from data import collect_file_paths
from preprocess import handle_missing_data
from utils import print_configuration


def main(args):
    feature_params = args.feature_params
    fex = FeatureExtractor(**feature_params)

    # TODO: Logger
    print(f"Checking marker files in {args.dataset_root}")
    print(f"target-nan-file: {args.target}")
    print(f"override: {args.override}")
    print("feature-params: {")
    for key in [k for k in feature_params.keys() if k in ["marker_set", "seq_len", "max_nan_seq"]]:
        print(f"\t{key} : {feature_params[key]}")
    print("}\n")

    # Get all files to figure out which ones to exclude with current feature configuration
    _, marker, _ = collect_file_paths(args.dataset_root, exclude=None, extracted=False)

    if not args.override or os.path.exists(args.target):
        with open(args.target, "w") as f:
            f.write("") # clear file (in case it exists)

        nan_counter = 0
        first = True  # The first line (turn-name) does not get a preceding newline
        for i, marker_filename in enumerate(marker):
            mf = fex.marker_features(marker_filename, extracted=False)
            mf_sections, _ = handle_missing_data(mf, min_frames=feature_params["seq_len"], max_nan_seq=feature_params["max_nan_seq"])
            if len(mf_sections) == 0:  # handle_missing_data returns empty arrays if there is no usable non-nan-sequence in markerfile
                nan_counter += 1
                with open(args.target, "a") as f:
                    if not first:
                        f.write("\n")
                    turn_name = marker_filename.split("/")[-1].split(".")[0]
                    f.write(turn_name)
                first = False

            print(f"\r{i: 4d}/{len(marker)} files checked", end="")
        print("\ndone.")
        print(f"{nan_counter} files are unusuable with the current feature configuration")

    else:
        print(f'target file "{args.target}" already exists. You can call with override or change target file name.')


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("-o", "--override", default=False, action="store_true")
    parser.add_argument("--target", default="nanfilesTEST.txt", type=str, required=False, help="path to target file")
    parser.add_argument("--dataset-root",  type=str, required=False, help="path to dataset")
    parser.add_argument("--feature-params", default='{}', help="yaml/json of wanted feature parameters (marker_set, t_step, ...)")
    parser.add_argument("--params-file", default='params.yaml', type=pathlib.Path, help="file (yaml or json) for feature parameters (marker_set, t_step, ...)")
  
    args_ , remaining = parser.parse_known_args()

    with open(args_.params_file, "r") as f:
        params_ = yaml.safe_load(f)

    feature_params_ = params_["feature_params"]
    feature_params_.update(yaml.safe_load(args_.feature_params))
    args_.feature_params = feature_params_

    if not args_.dataset_root:
        args_.dataset_root = params_["dataset_root"]

    main(args_)
        


# Usage:
# python write_nan_turns.py --target targetfile.txt --feature-params="{'seq_len':100}"

