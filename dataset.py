#!/usr/bin/env python

import random
import time 

import numpy as np
import torch
from torch.utils.data import Dataset, DataLoader
import matplotlib.pyplot as plt
import scipy
import pandas as pd

from data import collect_file_paths
from feature_extractor import FeatureExtractor
from preprocess import handle_missing_data


np.random.seed(0)
random.seed(0)
torch.manual_seed(0)


class SyncLipDataset(Dataset):
    def __init__(self, root_dir, train=True, extracted=False, exclude=None, emotions=True, **feature_params):
        super(SyncLipDataset, self).__init__()
        self.root_dir = root_dir
        self.train = train
        self.extracted = extracted
        self.emotions = emotions
        self.feature_params = {} if feature_params is None else feature_params

        # NOTE: with t_step=0.0083125 you'd need 72 frames for 1 sec
        self.seq_len = self.feature_params.get('seq_len')

        sessions = [5] if not train else list(range(1, 5))
        audio, marker, emo = collect_file_paths(self.root_dir, sessions=sessions, extracted=self.extracted, exclude=exclude)
        self.audio = audio
        self.marker = marker
        self.emo = emo

        self.fex = FeatureExtractor(**feature_params, extracted=self.extracted)

    def __len__(self):
        return len(self.audio)

    def __getitem__(self, idx):
        ''' Returns audio-features, marker-features, emotion-vector and mismatched-marker-features. '''
        turn_name = self.audio[idx].split("/")[-1].split(".")[0]
        if not self.extracted and self.emo[idx]["turn_name"] != turn_name:
            raise ValueError("Mismatched turn names: {self.emo[idx]['turn_name']} != {turn_name}")

        # Marker features
        mf = self.fex.marker_features(self.marker[idx], extracted=self.extracted)

        if self.seq_len is not None:  # If sequence length is given, mf is sliced, else it stays complete.
            mf_start = mf.index.values[0]
            mfs, idx_ranges = handle_missing_data(mf, min_frames=self.seq_len)

            block_idx = random.randint(0, len(mfs)-1)
            idx_range = idx_ranges[block_idx]
            mf = mfs[block_idx]

            max_idx = max(0, len(mf)-1 - self.seq_len)
            starting_frame = random.randint(0, max_idx)   # starting index has overlap from beginning included
            mf = mf.iloc[starting_frame: starting_frame+self.seq_len]

        # Audio features
        af = self.fex.audio_features(self.audio[idx], end=mf.index[-1], extracted=self.extracted)

        if self.seq_len is not None:  # If sequence length is given, af is sliced, else it stays complete.
            audio_starting_frame = starting_frame+idx_range[0]
            af = af.iloc[audio_starting_frame: audio_starting_frame+self.seq_len]

        # Emotion features
        if self.emotions:
            e = self.fex.emotion_features(self.emo[idx], extracted=self.extracted)
        else: 
            e = np.zeros(shape=(6,))

        if self.seq_len is not None:  # If sequence length is given, e is expanded
            e = np.tile(e, (self.seq_len, 1))

        # Mismatched marker features
        fake_idx = random.randint(0, len(self)-1)
        fake_mf = self.fex.marker_features(self.marker[fake_idx], extracted=self.extracted)
        if self.seq_len is not None:
            mf_start = fake_mf.index.values[0]
            mfs, idx_ranges = handle_missing_data(fake_mf, min_frames=self.seq_len)

            block_idx = random.randint(0, len(mfs)-1)
            fake_mf = mfs[block_idx]
            idx_range = idx_ranges[block_idx]

            max_idx = max(0, len(fake_mf)-1 - self.seq_len)
            starting_frame = random.randint(0, max_idx)
            fake_mf = fake_mf.iloc[starting_frame: starting_frame+self.seq_len]

        # Mismatched emotion features
        if self.emotions:
            fake_idx = random.randint(0, len(self)-1)
            fake_e = self.fex.emotion_features(self.emo[fake_idx], extracted=self.extracted)
        else: 
            fake_e = np.zeros(shape=(6,))
        if self.seq_len is not None:
            fake_e = np.tile(fake_e, (self.seq_len, 1))


        return af.values, mf.values, e, fake_mf.values, fake_e



def get_dataloader(dataset_root, batch_size, num_workers=0, train=True, 
                   extracted=False, exclude=None, **feature_params):
    ''' Returns a DataLoader object with given parameters. '''
    ds = SyncLipDataset(dataset_root, train=train, extracted=extracted, exclude=exclude, **feature_params)
    dl = DataLoader(ds, shuffle=train, num_workers=num_workers, batch_size=batch_size)#, drop_last=train)
    return dl
    

# Usage
#marker_set = ["MOU1","MOU2","MOU3","MOU4","MOU5","MOU6","MOU7","MOU8","RC1","RC4","LC1","LC4","CH1","CH2","CH3"]
#egemaps_set = ["F0semitoneFrom27.5Hz_sma3nz", "jitterLocal_sma3nz", "F1frequency_sma3nz", "F2frequency_sma3nz", "F3frequency_sma3nz", "F1bandwidth_sma3nz",
#               "shimmerLocaldB_sma3nz", "Loudness_sma3", "HNRdBACF_sma3nz", 
#               "alphaRatio_sma3", "hammarbergIndex_sma3", "slope0-500_sma3", "slope500-1500_sma3", 
#                  "F1amplitudeLogRelF0_sma3nz", "F2amplitudeLogRelF0_sma3nz", "F3amplitudeLogRelF0_sma3nz", "logRelF0-H1-H2_sma3nz", "logRelF0-H1-A3_sma3nz" ]
#
#ds = SyncLipDataset(root_dir="../dataset/IEMOCAP_full_release/", marker_set=marker_set, egemaps_set=egemaps_set, extracted=False, exclude="./nanturns.txt")
##ds = SyncLipDataset(root_dir="./dataset/feature_data_new/", feature_params={"marker_set":marker_set}, extracted=True, exclude="./nanturns.txt")
#dl = DataLoader(ds, shuffle=False, num_workers=0, batch_size=1)
#
#for i, data in enumerate(dl):
#    print(f"\r{(i+1):04d} ({100*(i+1)/len(dl):.0f}%)\t", end="")
#

