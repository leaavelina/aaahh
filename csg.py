#!/usr/bin/env python
import math

import torch
import torch.nn as nn


class Generator(nn.Module):
    def __init__(self, input_size, hidden_size, output_size, num_layers):
        super(Generator, self).__init__()

        self.lstm = nn.LSTM(input_size=input_size, hidden_size=hidden_size, 
                            num_layers=num_layers, bidirectional=True, batch_first=True)
        self.linear = nn.Linear(in_features=2*hidden_size, out_features=output_size)

    def forward(self, inputs, states=None):
        output, states = self.lstm(inputs, states)
        output = self.linear(output)
        return output, states


class Discriminator(nn.Module):
    def __init__(self, input_size, hidden_size, output_size, num_layers):
        super(Discriminator, self).__init__()

        self.lstm = nn.LSTM(input_size=input_size, hidden_size=hidden_size,
                            num_layers=num_layers, bidirectional=True, batch_first=True)
        self.linear = nn.Linear(in_features=2*hidden_size, out_features=output_size)
        self.sig = nn.Sigmoid()


    def forward(self, inputs, states=None):
        output, states = self.lstm(inputs, states)
        output = self.linear(output)
        output = self.sig(output)
        return output, states



def init_weights(m):
    ''' initialize weights of a nn.Module '''
    if isinstance(m, nn.LSTM):
        for layer in m._all_weights:
            rev = True if "rev" in layer[0] else False
            weights = [m.__getattr__(w) for w in layer if 'weight' in w]
            bias = [m.__getattr__(b) for b in layer if 'bias' in b]

            yih = 6/(m.input_size + m.hidden_size)**(0.5)
            nn.init.uniform_(weights[int(rev)], a=-yih, b=yih)

            yhh = 6/(2*m.hidden_size)**(0.5)
            nn.init.uniform_(weights[int(rev)], a=-yhh, b=yhh)

            for b in bias:
                nn.init.constant_(b, 0.0)

    if isinstance(m, nn.Linear):
        if m.weight is not None:
            #n = m.in_features
            #nn.init.normal_(m.weight.data, mean=0.0, std=(2/n)**(0.5))
            y = 6/(m.in_features + m.out_features)**(0.5)
            nn.init.uniform_(m.weight.data, a=-y, b=y)

        if m.bias is not None:
            nn.init.constant_(m.bias.data, 0.0)


def freeze_l0(module):
    for name, param in module.named_parameters():
        if "l0" in name:
            param.requires_grad=False
def unfreeze_l0(module):
    for name, param in module.named_parameters():
        if "l0" in name:
            param.requires_grad=True


# XAVIER UNIFORM
#nn.init.xavier_uniform_(l) #, gain=2**(0.5))
# UNIFORM (RULE)
#y = 6/(m.hidden_size + nextlayer.hidden_size)**(0.5)
#nn.init.uniform_(l, a=-y, b=y)
# NORMAL
#nn.init.normal_(l)

